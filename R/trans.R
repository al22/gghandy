#' @importFrom scales log_trans
#' @importFrom scales trans_new
NULL

#' Mirrored log1p breaks (integer breaks on mirrored log1p-transformed scales).
#'
#' @param n desired number of breaks in each direction (minus and plus)
#' @param base base of logarithm to use
#' @export
#' @examples
#' mirrorlog1p_breaks()(c(1, 1e6))
#' mirrorlog1p_breaks()(c(1, 1e4))
#' mirrorlog1p_breaks()(c(-1e4, 1e4))
mirrorlog1p_breaks <- function(n = 5, base = 10)
{
  function(x) {
    if (any(x < 0) && any(x > 0)) {
      brks <- c(-scales::log_breaks(n = n, base = base)(-x[x < 0] + 1),
                0,
                scales::log_breaks(n = n, base = base)(x[x > 0] + 1))
    } else {
      if (any(x < 0)) {
        brks <- -scales::log_breaks(n = n, base = base)(-x[x < 0] + 1)
      } else {
        brks <- scales::log_breaks(n = n, base = base)(x[x > 0] + 1)
      }
      if (any(x == 0)) {
        brks <- c(0, brks)
      }
    }
    return(sort(brks))
  }
}

#' Mirrored Log1p Transformation.
#'
#' @param base base of logarithm
#' @param n integer. number of breaks
#' @aliases mirrorlog1p_trans mirrorlog1p10_trans mirrorlog1p2_trans
#' @export mirrorlog1p_trans mirrorlog1p10_trans mirrorlog1p2_trans
mirrorlog1p_trans <- function(base = exp(1), n = 5) {
  trans <- function(x) ifelse(x > 0, log(x + 1, base),
                       ifelse(x < 0, -log(-x + 1, base),
                                     0))
  inv <- function(x) ifelse(x > 0, base ^ x - 1,
                     ifelse(x < 0, -(base ^ (-x) - 1),
                                   0))

  scales::trans_new(
    paste0("mirrorlog1p-", format(base)),
    trans,
    inv,
    mirrorlog1p_breaks(n = n, base = base),
    ##trans_breaks(trans, inv),
    domain = c(-Inf, Inf))
}
mirrorlog1p10_trans <- function() {
  mirrorlog1p_trans(10)
}
mirrorlog1p2_trans <- function() {
  mirrorlog1p_trans(2)
}




##' calculate nice minor breaks on a log scale
##'
##' original idea taken from
##' \url{https://stackoverflow.com/a/33179099}.
##'
##' \code{base} is assumed to be integer.
##'
##' Assumes major breaks base^i, base^(i+1), base^(i+2), ...
##'
##' This function is for the positive major breaks.
##'
##' @param major_breaks numeric.  major breaks on natural (not log!)
##'   scale.  See details.
##' @param base numeric.  base used for the logarithm.  Assumed to be
##'   integer.  Defaults to 10.
##' @return numeric.  vector of minor breaks
##' @author Andreas Leha, original idea by gvrocha
##' @export
logminorbreaks <- function(major_breaks, base = 10)
{
  ## how many minor breaks to produce (base assumed to be integer)
  nmb <- base - 1
  ## make integer -- just to be sure
  nmb <- round(nmb)

  major_breaks <- log(major_breaks, base = base)
  major_breaks <- c(min(major_breaks) - 1, major_breaks)
  n_major      <- length(major_breaks)
  minor_breaks <-
    rep(log(1:nmb, base), times = n_major) +
    rep(major_breaks, each = nmb)
  return(minor_breaks)
}


##' calculate nice minor breaks on a (mirrored) log scale
##'
##' This is a wrapper around \code{\link{logminorbreaks}} that also
##'   works on the 'minus side'
##' @param base numeric. base of the logarithm
##' @param n numeric. number of major breaks
##' @author Andreas Leha
##' @aliases mirrorlogX_minor_intbreak mirrorlog10_minor_intbreak mirrorlog2_minor_intbreak
##' @export mirrorlogX_minor_intbreak mirrorlog10_minor_intbreak mirrorlog2_minor_intbreak
##' @examples
##' mirrorlogX_minor_break(n = 2, base = 10)(c(-exp(3:1), 0, exp(1:3)))
##' mirrorlogX_minor_break(n = 2, base = 2)(c(-exp(3:1), 0, exp(1:3)))
mirrorlogX_minor_intbreak <- function(n = 5, base = 10) {
  function(x) {
    major_breaks <- mirrorlog1p_intbreaks(n = n, base = base)(x)
    if (any(major_breaks < 0) && any(major_breaks > 0)) {
      minor_breaks_plus  <- logminorbreaks(sort( major_breaks[major_breaks > 0]), base = base)
      minor_breaks_minus <- logminorbreaks(sort(-major_breaks[major_breaks < 0]), base = base)
      minor_breaks <- c(rev(-base^minor_breaks_minus),
                             base^minor_breaks_plus)
    } else {
      if (any(major_breaks < 0)) {
        minor_breaks <- logminorbreaks(-major_breaks, base = base)
        minor_breaks <- rev(-base^minor_breaks)
      } else {
        minor_breaks <- logminorbreaks(major_breaks, base = base)
        minor_breaks <- base^minor_breaks
      }
    }
    return(minor_breaks)
  }
}
mirrorlog10_minor_intbreak <- mirrorlogX_minor_intbreak(n = 5, base = 10)
mirrorlog2_minor_intbreak <- mirrorlogX_minor_intbreak(n = 5, base = 2)


#' Mirrored log1p breaks (integer breaks on mirrored log1p-transformed scales).
#'
#' Only breaks of the form base^i where i is an integer are produced.
#' @param n desired number of breaks in each direction (minus and plus)
#' @param base base of logarithm to use
#' @export
#' @examples
#' mirrorlog1p_breaks()(c(1, 1e6))
#' mirrorlog1p_breaks()(c(1, 1e4))
#' mirrorlog1p_breaks()(c(-1e4, 1e4))
mirrorlog1p_intbreaks <- function(n = 5, base = 10)
{
  function(x) {
    if (any(x < 0) && any(x > 0)) {
      ## number of breaks in each direction
      nb <- n - 1        ## we subtract 1 because one break is going to be at 0
      nb <- nb / 2       ## we divide by 2 because we want the same number of breaks in each direction
      nb <- ceiling(nb)  ## we round up
      nb <- nb + 1       ## we add one as one break tends to be over the maximum of x and might not be visible
      max_p <- max(x)
      max_m <- -min(x)
      upper_p <- ceiling(log(max_p, base = base))
      upper_m <- ceiling(log(max_m, base = base))
      plus_breaks <- upper_p:(upper_p - nb + 1)
      plus_breaks <- base ^ plus_breaks
      plus_breaks <- rev(plus_breaks)
      minus_breaks <- upper_m:(upper_m - nb + 1)
      minus_breaks <- base ^ minus_breaks
      minus_breaks <- -(minus_breaks)
      brks <- c(
        minus_breaks,
        0,
        plus_breaks
      )
    } else {
      if (any(x < 0)) {
        max <- -min(x)
        upper <- ceiling(log(max, base = base))
        brks <- upper:(upper - n + 1)
        brks <- base ^ brks
        brks <- rev(brks)
      } else {
        max <- max(x)
        upper <- ceiling(log(max, base = base))
        brks <- upper:(upper - n + 1)
        brks <- base ^ brks
        brks <- rev(brks)
      }
      if (any(x == 0)) {
        brks <- c(0, brks)
      }
    }
    return(sort(brks))
  }
}

#' Mirrored Log1p Transformation with 'base^integer breaks'
#'
#' See \code{\link{mirrorlog1p_intbreaks}} for details.
#' @param base base of logarithm
#' @param n integer. number of breaks
#' @aliases mirrorlog1p_intbreak_trans mirrorlog1p10_intbreak_trans mirrorlog1p2_intbreak_trans
#' @export mirrorlog1p_intbreak_trans mirrorlog1p10_intbreak_trans mirrorlog1p2_intbreak_trans
mirrorlog1p_intbreak_trans <- function(base = exp(1), n = 5) {
  trans <- function(x) ifelse(x > 0, log(x + 1, base),
                       ifelse(x < 0, -log(-x + 1, base),
                                     0))
  inv <- function(x) ifelse(x > 0, base ^ x - 1,
                     ifelse(x < 0, -(base ^ (-x) - 1),
                                   0))

  scales::trans_new(
    paste0("mirrorlog1p-",format(base), "-intbreaks"),
    trans,
    inv,
    mirrorlog1p_intbreaks(n = n, base = base),
    ##trans_breaks(trans, inv),
    domain = c(-Inf, Inf))
}
mirrorlog1p10_intbreak_trans <- function() {
  mirrorlog1p_intbreak_trans(10)
}
mirrorlog1p2_intbreak_trans <- function() {
  mirrorlog1p_intbreak_trans(2)
}
