% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/gghandy-package.r
\name{grid.newpagedraw}
\alias{grid.newpagedraw}
\title{Draw a grid 'Plot' on a Fresh Canvas}
\usage{
grid.newpagedraw(p)
}
\arguments{
\item{p}{grob to draw}
}
\value{
Nothing.  Called for side effects (plotting) only
}
\description{
Draw a grid 'Plot' on a Fresh Canvas
}
\author{
Andreas Leha
}
