#' gghandy
#'
#' @name gghandy
#' @docType package
#' @importFrom labeling extended
#' @importFrom GGally ggpairs
#' @importFrom GGally putPlot
#' @importFrom grImport PostScriptTrace
#' @importFrom grImport readPicture
#' @importFrom grImport grid.picture
#' @importFrom grImport pictureGrob
#' @importFrom descutils prettyPvalues
NULL

##' Read PDF and convert to ggplot object
##'
##' @param pdfpath character.  Path to pdf file.
##' @param page integer. the page to plot.  Defaults to 1.
##' @return grob containing the pdf
##' @author Dr. Andreas Leha
##' @export
ggpdf <- function(pdfpath, page = 1)
{
  ## PostScriptTrace cannot handle complicated file names
  ## (and this is cleaner)
  ## so we use a temp file
  tmppsfile <- tempfile(fileext = ".ps")

  ## convert pdf to ps
  cmd <- paste("pdftops", "-origpagesizes", "-f", page, "-l", page, pdfpath, tmppsfile)
  exitstatus <- system(cmd)
  if (exitstatus != 0) stop("pdftops failed")

  ## trace the ps file
  xmlfile <- gsub("ps$", "xml", tmppsfile)
  PostScriptTrace(tmppsfile, xmlfile)

  ## read in the traced image
  pdfplot <- readPicture(xmlfile)

  if (FALSE) {
    ## have a look
    grid.newpage()
    grid.picture(pdfplot)
  }

  ## make into grob
  pdfgrob <- pictureGrob(pdfplot)

  return(pdfgrob)
}

##' Convert grob to ggplot
##'
##' Converts \code{grob} object to \code{ggplot} objects by plotting them via
##'   \code{\link[ggplot2]{annotation_custom}} onto an empty \code{ggplot}.
##' @param gr grob to be converted
##' @return ggplot
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## create grob (by converting ggplot to grob)
##' p <- qplot(1:10, 1:10)
##' class(p)
##' gr <- ggplotGrob(p)
##' class(gr)
##'
##' ## convert back to ggplot
##' gr_p <- grobGgplot(gr)
##' class(gr_p)
grobGgplot <- function(gr)
{
  ## create empty plot
  p <- ggplot() + theme_void()##ggblank(transparentplot = TRUE)
  ## plot grob onto empty (convert to ggplot)
  p <- p + annotation_custom(gr)

  return(p)
}


##' Visualise Missingness in Data
##'
##' Inspired by \code{Ameilia::missmap}
##' @param dat data.frame to visualise
##' @param sort logical.  If TRUE both, columns and rows, are sorted
##'   by the number of missing values.  Defaults to FALSE
##' @param sort.cols logical.  If TRUE columns (variables) are sorted
##'   by the number of missing values.  Defaults to FALSE
##' @param sort.rows logical.  If TRUE rows (samples) are sorted
##'   by the number of missing values.  Defaults to FALSE
##' @return ggplot
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## use mtcars
##' dat <- mtcars
##' ## generate some missing values
##' for (i in 1:15) dat[sample(nrow(dat), 1), sample(ncol(dat), 1)] <- NA
##'
##' ## plot
##' ggmissmap(dat)
##' ## ordered by number of NAs
##' ggmissmap(dat, sort = TRUE)
ggmissmap <- function(dat, sort = FALSE, sort.cols = FALSE, sort.rows = FALSE)
{
  if (sort || sort.cols) {
    colorder <- dat %>% summarise_all(.funs = funs("sumNA" = sum(is.na(.)))) %>% unlist %>% sort(decreasing = TRUE)
    names(colorder) <- gsub("_sumNA$", "", names(colorder))
  } else {
    colorder <- dat %>% colnames %>% setNames(colnames(dat))
  }

  if (is.null(rownames(dat))) {
    dat <- dat %>%
      mutate(.sample = 1:n())
  } else {
    dat <- dat %>% tibble::rownames_to_column(".sample")
  }

  if (sort || sort.rows) {
    roworder <- dat %>% group_by(.sample) %>% do(sumNA = sum(is.na(.))) %>% mutate(sumNA = unlist(sumNA))
    roworder <- roworder$sumNA %>% setNames(roworder$.sample) %>% sort
  } else {
    roworder <- 1:nrow(dat) %>% setNames(dat$.sample)
  }


  p <- dat %>%
    mutate(.sample = factor(.sample, levels = names(roworder))) %>%
    gather(variable, value, one_of(setdiff(colnames(dat), ".sample"))) %>%
    mutate(variable = factor(variable, levels = names(colorder))) %>%
    mutate(missing = ifelse(is.na(value), "missing", "observed")) %>%
    ggplot(aes(x = variable, y = .sample, fill = missing)) +
    geom_tile() +
    theme_minimal() +
    scale_x_discrete(expand = c(0,0)) +
    scale_y_discrete("sample", expand = c(0,0)) +
    scale_fill_manual(values = c("darkred", "gray")) +
    theme(legend.position = "top",
          legend.title = element_blank(),
          axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.4),
          axis.title.x = element_blank())

  return(p)
}

##' Version of GGally::ggpairs for pairwise correlations
##'
##' Customized version of \code{GGally::ggpairs} that has
##' (1) densities on the diagonal,
##' (2) scatter plots in the lower triangle, and
##' (3) heatmap with correlation coefficient and p value in the
##' upper triangle
##' @param data data.frame containing the \code{columns}
##' @param columns vector of columns in data to consider
##' @param title character.  string to put as title.  defaults to ""
##' (i.e. no title)
##' @param robustcorrelation logical or character.  If TRUE, all
##' columns will be checked for outliers and these will be set to NA.
##' If a character vector, only the given columns will be affected.
##' Defaults to \code{FALSE}
##' @param robustscatter logical.  Only effective if
##' \code{robustcorrelation != FALSE}.  If TRUE data points removed
##' because of \code{robustcorrelation} will be removed from the
##' scatter plots as well.  Defaults to \code{FALSE}.
##' @param method character vector.  Allowed entries are possible
##' values for the \code{method} argument of
##' \code{link{stats::cor.test}} ("pearson", "kendall", "spearman").
##' If named, only the given columns in \code{data} are
##' affected.  If unnamed all columns of \code{data} are affected and
##' \code{method} will be re-cycled.
##' @param p.adj.method character vector.  See
##' \code{?p.adjust.methods} for allowed values.  Defaults to \code{"holm"}.
##' @param p.adj.by character in c("all", "row", "col").  Defines the
##' hypothesis space to adjust over.  'row' and 'col' are identical
##' and allow only "bonferroni" for \code{p.adj.by}.  Defaults to 'all'
##' @param clustercolumns logical.  If \code{TRUE} the columns (and
##' rows...) will be clustered by correlation.  Defaults to \code{FALSE}
##' @param show.p logical.  If \code{TRUE} the raw p values will be
##' shown in the resulting plot.  Defaults to \code{TRUE}.
##' @param show.p.adj logical.  If \code{TRUE} the adjusted p values
##' are shown in the resulting plot.  Defaults to \code{TRUE}.
##' @param textsize_tiles numeric.  Size of text for upper triangle
##' tiles.  Defaults to \code{3}.
##' @param colour_scale_tiles character in \code{c("fixed",
##' "free")}. if \code{"fixed"} the colours will be scaled from -1 to
##' 1.  Defaults to \code{"fixed"}
##' @param replaceDiagonal logical.  if \code{TRUE} the diagonal will
##' be sipmle density plots.  Defaults to \code{FALSE}
##' @param diagcolours optional vector of colours to use as foreground
##' of the diagonal density plots
##' @param diagbgs optional vector of colours to use as background
##' of the diagonal density plots
##' @param axis.text logical.  Defaults to \code{TRUE}
##' @param axis.labels logical.  Defaults to \code{TRUE}
##' @param center.labels logical. Defaults to \code{FALSE}
##' @param alpha numeric.  Transperancy of the scatter plot points.
##' Defaults to 0.3
##' @param scatter_groups column of \code{data} containing the values to group by
##' @param scatter_smooth logical or character.  If not FALSE this will add a
##' \code{\link{ggplot2::geom_smooth}} layer to the scatter plots.  If character, this
##' will be passed to \code{\link{ggplot2::geom_smooth}} as value for
##' the \code{method} parameter (e.g. "lm", "rlm", "loess").  TRUE is
##' translated into "auto".  Defaults to FALSE.
##' @param densityAsScatter logical. if \code{TRUE} 2dDensity plots
##' are generated instead of the scatter plots.
##' @param densityWithPoints logical. if \code{TRUE} the 2d density
##' plot includes 'outliers' as points.  Careful: takes a lot of time.
##' Defaults to \code{FALSE}.
##' @param verbose numeric. level of verbosity
##' @param ... additional args are passed to \code{GGally::ggpairs}
##' @return ggplot object.
##' @author Andreas Leha, user20650
##' @export
##' @examples
##' \dontrun{
##' ggcorpairs(mtcars, c("mpg", "disp", "hp", "drat", "wt", "qsec"))
##'
##' ## add cyl and use kendall there
##' ggcorpairs(mtcars, c("mpg", "disp", "hp", "drat", "wt", "qsec", "cyl"), method = c(cyl = "kendall"))
##'
##' ## smoothing
##' ## default "auto" smoothing
##' ggcorpairs(mtcars,
##'            c("mpg", "disp", "hp", "drat", "wt", "qsec", "cyl"),
##'            method = c(cyl = "kendall"),
##'            scatter_smooth = TRUE)
##' ## lm smoothing
##' ggcorpairs(mtcars,
##'            c("mpg", "disp", "hp", "drat", "wt", "qsec", "cyl"),
##'            method = c(cyl = "kendall"),
##'            scatter_smooth = "lm")
##'
##' ## add title
##' ggcorpairs(mtcars, c("mpg", "disp", "hp", "drat", "wt", "qsec"), title = "mtcars")
##'
##' ## move the variable labels to the center
##' ggcorpairs(mtcars, c("mpg", "disp", "hp", "drat", "wt", "qsec"),
##'            axis.labels   = FALSE,
##'            center.labels = TRUE)
##' }
ggcorpairs <- function(
                       data,
                       columns,
                       title = "",
                       robustcorrelation = FALSE,
                       robustscatter = FALSE,
                       method = "pearson",
                       p.adj.method = "holm",
                       p.adj.by = "all",
                       clustercolumns = FALSE,
                       show.p = TRUE,
                       show.p.adj = TRUE,
                       textsize_tiles = 3,
                       colour_scale_tiles = "fixed",
                       replaceDiagonal = FALSE,
                       diagcolours = NULL,
                       diagbgs = NULL,
                       axis.text = TRUE,
                       axis.labels = TRUE,
                       center.labels = FALSE,
                       alpha=0.3,
                       scatter_groups = NULL,
                       scatter_smooth = FALSE,
                       densityAsScatter = FALSE,
                       densityWithPoints = FALSE,
                       verbose = 0,
                       ...
                       )
{

  ## http://blog.felixriedel.com/2013/05/pairwise-distances-in-r/
  vectorized_pdist <- function(A,B)
  {
    an = apply(A, 1, function(rvec) crossprod(rvec,rvec))
    bn = apply(B, 1, function(rvec) crossprod(rvec,rvec))

    m = nrow(A)
        n = nrow(B)

    tmp = matrix(rep(an, n), nrow=m)
    tmp = tmp +  matrix(rep(bn, m), nrow=m, byrow=TRUE)
    sqrt( tmp - 2 * tcrossprod(A,B) )
  }

  ## local geom_scatter + geom_smooth function
  ## http://stackoverflow.com/a/35088740
  scattersmooth <- function(data, mapping, pts=list(), smt=list(), ...) {
    res <-
      ggplot2::ggplot(data = data, mapping = mapping, ...)

    if (densityAsScatter) {
      ## https://stackoverflow.com/a/20216544
      res <- res +
        ggplot2::stat_density2d(mapping = aes(fill=..density..^0.25, alpha=1),
                                geom="tile", contour=FALSE)

      ## filter for points in low-density regions
      aaa <- ggplot_build(res + geom_point())
      ccc <- vectorized_pdist((aaa$data[[2]] %>% dplyr::select(x, y) %>% as.matrix),
                              (aaa$data[[1]] %>% dplyr::select(x, y) %>% as.matrix))
      ddd <- apply(ccc, 1, which.min)
      bbb <- data_frame(x = aaa$data[[2]]$x, y = aaa$data[[2]]$y, d = aaa$data[[1]]$density[ddd])
      outlier_data <- bbb %>% dplyr::filter(d^0.25 < 0.3)

      res <- res +
        ggplot2::geom_point(aes(x = x, y = y), inherit.aes = FALSE, size=0.5, data = outlier_data)
      ##ggplot2::stat_density2d(mapping = aes(fill=..density..^0.25, alpha=ifelse(..density..^0.25<0.4,0,1)),
      ##geom="tile", contour=FALSE)
    } else {
      res <- res +
        do.call(ggplot2::geom_point, pts)
    }

    res <- res +
      do.call(ggplot2::geom_smooth, smt)

    if (densityAsScatter) {
      res <- res +
        scale_fill_gradientn(colours = colorRampPalette(c("white", blues9))(256))
    }

    ## style
    res <- res + ggplot2::theme_light()

    return(res)
  }

  ## set scatter function
  scatter_fun <- if (identical(scatter_smooth, FALSE)) "points" else scattersmooth
  scatter_method <- if (identical(scatter_smooth, TRUE)) "auto" else scatter_smooth

  if (missing(columns)) columns <- colnames(data)

  ## what are the necessary columns
  toselect <-
    ## put the desired scatter_groups at the end
    if (!is.null(scatter_groups)) {
      columns <- setdiff(columns, scatter_groups)
      c(columns, scatter_groups)
    } else {
      columns
    }
  ## safety check
  toselect <- toselect %>% unique

  ## subset the data.frame
  ccc <- data[,toselect]

  ## convert to data.frame
  ccc <- data.frame(ccc, check.names = FALSE)

  ## parse method
  if (is.null(names(method))) {
    methods <- rep(method, length.out = length(columns))
    names(methods) <- columns
  } else {
    methods <- rep("pearson", length.out = length(columns))
    names(methods) <- columns
    methods[names(method)] <- method
  }
  ## make into matrix
  metmat <- matrix(rep("", length(columns)^2), nrow = length(columns))
  colnames(metmat) <- rownames(metmat) <- columns
  for (i in columns) {
    for (j in columns) {
      pcor <- unique(methods[c(i,j)])
      if (length(pcor) > 1) {
        pcor <- pcor[pcor != "pearson"]
        if (length(pcor) > 1) {
          pcor <- pcor[1]
        }
      }
      metmat[i,j] <- pcor
    }
  }
  if (verbose) print(metmat)

  ## remove outliers from both correlation and scatter
  if (robustcorrelation && robustscatter) {
    for (j in 1:ncol(ccc)) {
      ccc[,j][isOutlier(ccc[,j])] <- NA
    }
  }

  ## data ignoring the scattergroups
  nnn <- ccc[,columns]

  ## calculate all pairwise correlations
  corsdf <-
    adply(colnames(nnn), 1, function(v) adply(colnames(nnn), 1,  function(w) {
      testdf <- nnn[,c(v,w)]
      if (robustcorrelation && !robustscatter) {
        testdf[,v][isOutlier(testdf[,v])] <- NA
        testdf[,w][isOutlier(testdf[,w])] <- NA
      }
      cormet <- metmat[v,w]
      cors <- purrr::possibly(otherwise = list(estimate = NA, statistic = NA, p.value = NA),
                             .f=cor.test)(
                         as.numeric(testdf[[v]]),
                         as.numeric(testdf[[w]]),
                         method = cormet)
      data.frame(v=v,
                 w=w,
                 method = cormet,
                 r=cors$estimate,
                 t=cors$statistic,
                 p=cors$p.value)
    }))
  if (verbose) corsdf %>% print

  ## adjust p values
  corsdf <- corsdf %>%
    mutate(
      comparison = ifelse(as.character(v) < as.character(w), paste(v, w, sep = ":"), paste(w, v, sep = ":")),
      p.adj = p,
      p.adj = ifelse(v == w, NA, p.adj))
  if (p.adj.by == "all") {
    corsdf <- corsdf %>%
      mutate(
        p.adj = ifelse(duplicated(comparison), NA, p.adj),
        p.adj = p.adjust(p.adj, method = p.adj.method))
  } else if (p.adj.by %in% c("row", "col")) {
    if (p.adj.method != "bonferroni") stop("when adjusting row (or col) wise, only p.adj.method = 'bonferroni' allowed")
    if (p.adj.by == "row") {
      corsdf <- corsdf %>% group_by(v)
    } else {
      corsdf <- corsdf %>% group_by(w)
    }
    corsdf <- corsdf %>%
      mutate(p.adj = p.adjust(p.adj, method = p.adj.method)) %>%
      ungroup
  } else {
    stop("unsupported 'p.adjust.by' value")
  }
  corsdf <- corsdf %>% mutate(p.adj = ifelse(v == w, 0, p.adj))
  if (p.adj.by == "all") {
    p.adj <- corsdf %>% dplyr::select(p.adj, comparison) %>% filter(!is.na(p.adj))
    corsdf <- corsdf %>% dplyr::select(-p.adj) %>% left_join(p.adj, by = "comparison") %>% dplyr::select(-comparison)
  }
  if (verbose) corsdf %>% print

  ## cluster the columns if asked
  if (clustercolumns) {

    if (sum(is.na(corsdf$r)) > 1)
      warning(sum(is.na(corsdf$r)), " (out of ", nrow(corsdf), ") correlations uncomputable", "\n",
              "-> will be set to 0 for clustering")

    corsdf %>%
      mutate(
        ##r = abs(r),
        r = r - 1,
        r = -r,
        r = if_else(is.na(r), 0, r)) %>%
      dplyr::select(v, w, r) %>%
      tidyr::spread(w, r) %>%
      tibble::remove_rownames() %>%
      as.data.frame %>%
      tibble::column_to_rownames("v") %>%
      as.dist %>%
      hclust ->
      colclust

    nnn <- nnn[, colclust$order]
    ccc <- ccc[, c(columns[colclust$order], scatter_groups)]

  } else {

    colclust <- NULL

  }

  ## initial version
  ## cannot make GGally::ggpairs accept non-standard names
  ddd <- ccc %>% data.frame ## this will convert names to standard names
  ## TODO: ggpairs fails on logical columns

  if (axis.labels) {
    p_pairs <-
      GGally::ggpairs(ddd,
                      mapping=ggplot2::aes_string(colour = scatter_groups),
                      lower=list(continuous = GGally::wrap(scatter_fun, alpha = alpha, smt = list(method = scatter_method))),
                      title = title,
                      ...)
  } else {
    p_pairs <-
      GGally::ggpairs(ddd,
                      ##columnLabels = rep(" ", ncol(ddd)),
                      mapping=ggplot2::aes_string(colour = scatter_groups),
                      columnLabels = NULL,
                      lower = list(continuous = GGally::wrap(scatter_fun, alpha=alpha, smt = list(method = scatter_method))),
                      title = title,
                      ...)
  }

  ## style
  p_pairs <- p_pairs + ggplot2::theme_light()  + theme(panel.grid = element_blank())

  ## remove axis text if told to
  if (!axis.text)
    p_pairs <- p_pairs + theme(axis.text = element_blank(),
                               axis.ticks = element_blank())

  ## replace the text panels with heatmap tiles
  sizeCor = textsize_tiles
  for (i in 1:(ncol(nnn) - 1)) {
    if (verbose) print(i)
    for (j in (i+1):ncol(nnn)) {
      if (verbose) print(j)
      cors <- corsdf[corsdf$v==colnames(nnn)[i] &
                     corsdf$w==colnames(nnn)[j],]
      estname <- ifelse(cors$method == "pearson",
                        "r",
                 ifelse(cors$method == "kendall",
                        "τ",
                 ifelse(cors$method == "spearman",
                        "ρ",
                        "?")))
      cors$text <- paste0(estname, " = ", sprintf("%.2f", cors$r))
      if (show.p)
        cors$text <- cors$text %>% paste0("\n",
                                          ##"t = ", sprintf("%.2f", cors$t), "\n",
                                          descutils::prettyPvalues(cors$p, digits=3, lhs="p", orgbold=FALSE))
      if (show.p.adj)
        cors$text <- cors$text %>% paste0("\n",
                                          ##"t = ", sprintf("%.2f", cors$t), "\n",
                                          descutils::prettyPvalues(cors$p.adj, digits=3, lhs="p.adj", orgbold=FALSE))


      ## Plot
      p <- ggplot2::ggplot(cors, ggplot2::aes(x = 1, y = 1)) +
        ggplot2::geom_tile(ggplot2::aes(fill = r)) + ##"steelblue") +
        ggplot2::geom_text(ggplot2::aes(x = 1, y = 1, label = text, colour = round(r)
                                        ##alpha = factor(p.adj < 0.05, levels = c("FALSE", "TRUE")),
                                        ##fontface = as.numeric(factor(p.adj < 0.05, levels = c("FALSE", "TRUE")))),
                                        ),
                           alpha = ifelse(cors$p.adj < 0.05, 1, 0.3),
                           fontface = ifelse(is.na(cors$p.adj) | cors$p.adj >= 0.05, "plain", "bold"),
                           size = sizeCor, show.legend = FALSE) +
        ggplot2::scale_x_continuous(expand=c(0,0)) +
        ggplot2::scale_y_continuous(expand=c(0,0))

      if (colour_scale_tiles == "fixed") {
        p <- p +
          ggplot2::scale_fill_gradient2(limits=c(-1,1), low="#053061", mid="white", high="#67001F", na.value="#7F7F7FAA") +
          ggplot2::scale_colour_gradient2(limits = c(-1,1), low="white", mid="black", high="white")
      } else {
        r_limits <-
          corsdf %>%
          ungroup %>%
          dplyr::filter(v != w) %>%
          summarise(minr = min(r, na.rm = TRUE),
                    maxr = max(r, na.rm = TRUE))
        p <- p +
          ggplot2::scale_fill_gradient2(limits=c(r_limits$minr, r_limits$maxr), midpoint = (r_limits$minr + r_limits$maxr)/2,
                                        low="#053061", mid="white", high="#67001F", na.value="#7F7F7FAA") +
          ggplot2::scale_colour_gradient2(limits=c(r_limits$minr, r_limits$maxr), midpoint = (r_limits$minr + r_limits$maxr)/2,
                                          low="white", mid="black", high="white")
      }

      ## styling
      p <- p +
      ##ggplot2::scale_alpha_manual(values = c(0.3, 1), drop = FALSE) +
          ggplot2::theme_minimal() +
            ggplot2::theme(panel.grid   = ggplot2::element_blank(),
                           axis.ticks   = ggplot2::element_blank(),
                           axis.title.x = ggplot2::element_blank(),
                           axis.text.x  = ggplot2::element_blank(),
                           axis.title.y = ggplot2::element_blank(),
                           axis.text.y  = ggplot2::element_blank())

      p_pairs <- GGally::putPlot(p_pairs, p, i, j)
    }
  }

  ## ## density plots with coloured background
  ## if (!is.null(diagcolours)) {
  ##   for (i in 1:ncol(nnn)) {
  ##
  ##     p <- ggplot(nnn, aes_string(x=colnames(nnn)[i]))+
  ##       geom_line(stat="density")
  ##     p <- p +
  ##       theme_minimal() +
  ##       theme(axis.ticks = element_blank(),
  ##             axis.title = element_blank(),
  ##             axis.text  = element_blank(),
  ##             panel.background = element_rect(fill = diagcolours[i], colour=NA))
  ##
  ##     p_pairs <- GGally::putPlot(p_pairs, p, i, i)
  ##   }
  ## }

  ## density plots on the diagonal
  ##if (!is.null(diagcolours) || !is.null(diagbgs)) {
  if (replaceDiagonal) {
    for (i in 1:ncol(nnn)) {

      ver <- colnames(nnn)[i]

      p <- ggplot2::ggplot(nnn, ggplot2::aes_string(x=ver))

      if (!is.null(diagcolours) && !is.na(diagcolours[i])) {
        p <- p + ggplot2::geom_line(stat="density", colour = diagcolours[i], size=1.2)
      } else {
        p <- p + ggplot2::geom_line(stat="density", size=1.2)
      }

      if (center.labels) {
        annodat <- data.frame(x = Inf, y = Inf, label = ver)
        p <- p + ggplot2::geom_text(ggplot2::aes(x = x, y = y, label = label), data = annodat, hjust = 1, vjust = 2)
      }

      p <- p +
        ggplot2::theme_minimal() +
        ggplot2::theme(panel.grid   = ggplot2::element_blank(),
                       axis.ticks   = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank(),
                       axis.text    = ggplot2::element_blank()
                       )

      if (!is.null(diagbgs) && !is.na(diagbgs[i])) {
        p <- p + ggplot2::theme(panel.background = ggplot2::element_rect(fill = diagbgs[i], colour=NA))
      }

      p_pairs <- GGally::putPlot(p_pairs, p, i, i)
    }
  }
  ##}

  return(list(vars = colnames(nnn),
              cors = corsdf,
              colclust = colclust,
              plot = p_pairs))
}

##' rotate labels on GGally::pairs plots
##'
##' @param p_pairs ggmatrix.  result of GGally::pairs
##' @param labels character vector.  length the same as matrix dimension
##' @param labelsize numeric.  Defaults to 10
##' @param lwp numeric.  space for labels at the left of plots.
##'   Defaults to 3
##' @param bwp numeric.  space for labels at bottom of the plot.
##'   Defaults to 3
##' @return result of \code{customize.labels}.  called for side effects
##' @author Andreas Leha
##' @export
##' @examples
##' p <- ggcorpairs(mtcars,
##'                 c("mpg", "disp", "hp", "drat", "wt", "qsec"),
##'                 axis.labels = FALSE)$plot
##' ggcorpairsRotLabels(p, c("mpg", "disp", "hp", "drat", "wt", "qsec"),
##'                     lwp = 0.5, bwp = 0.5)
ggcorpairsRotLabels <- function(p_pairs, labels, labelsize=10, lwp=3, bwp=3)
{
  ## print the plot with space for the labels
  print(p_pairs,
        leftWidthProportion=lwp,
        bottomHeightProportion=bwp)

  ## add rotated labels
  customize.labels(p_pairs,
                   leftWidthProportion=lwp,
                   bottomHeightProportion=bwp,
                   left.opts = list(x=0.8, y=0.5, just=c('right', 'center'), rot=0, gp=list(fontsize=labelsize)),
                   bottom.opts = list(x=0.5, y=0.8, just=c('right', 'center'), rot=90, gp=list(fontsize=labelsize)),
                   varLabels = labels)
}




##' replicate ggplot's default colours
##'
##' from \url{http://stackoverflow.com/a/8197703}
##' @param n integer. the number of colours to generate
##' @return vector of length \code{n} with ggplot's default colours
##' @author John Colby
##' @export
##' @examples
##' ggpal(gg_colour_hue(2))
gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}


##' robustify names for aes_string
##'
##' changes a string so it is enclosed in back-ticks.
##' This can be used to make column names that have spaces (blanks)
##' or non-letter characters acceptable to ggplot2.
##'
##' This version of the function is vectorized with sapply.
##'
##' From: http://stackoverflow.com/a/30152108
##' @param x character to be robustified
##' @return character.  robustified version of x
##' @author Kevan Doyle
##' @export
ggname <- function(x) {
  if (class(x) != "character") {
    return(x)
  }
  y <- sapply(x, function(s) {
    if (identical(make.names(s), s)) {
      s <- s
  } else {
    if (!grepl("^`", s)) {
      s <- paste("`", s, sep="", collapse="")
    }
    if (!grepl("`$", s)) {
      s <- paste(s, "`", sep="", collapse="")
    }
  }
  return(s)
  })
  y
}

##' Interface to Create a textGrob
##'
##' @param lab character.  The text to use.
##' @param rot numeric.  Angle of rotation in degrees.  Defaults to 45.
##' @param cex numeric.  Scaling factor.  Defaults to 5.
##' @param col coulor specicication.  Defaults to "gray90".
##' @return A \code{textGrob} object.
##' @author Andreas Leha
##' @export
watermarkGrob <- function(lab = "work in progress", rot = 45, cex = 5, col = "gray90")
{
  ## automatic scalung does not work without an open device
  ##cex <- grid::convertUnit(grid::unit(1,"npc"), "mm", val=TRUE) /
  ##  grid::convertUnit(grid::unit(1,"grobwidth", grid::textGrob(lab, rot = rot)), "mm", val=TRUE)

  grid::textGrob(lab, rot=rot, gp = grid::gpar(cex = cex, col = col, alpha = 0.5))
}

##' Create an Annotation Layer for ggplot
##'
##' This is intended for text annotation to add to ggplot objects.
##'   The default will create a layer containing 'work in progress'
##'   scaled up and rotated by 45 degrees.
##' @param lab character.  The text to use.
##' @param rot numeric.  Angle of rotation in degrees.  Defaults to 45.
##' @param cex numeric.  Scaling factor.  Defaults to 5.
##' @param col coulor specicication.  Defaults to "gray90".
##' @return ggplot2::annotation_custom layer
##' @author Andreas Leha
##' @export
watermarkAnnotation <- function(lab = "work in progress", rot = 45, cex = 5, col = "gray90")
{
  ## the text grob
  watermarkText <- watermarkGrob(lab = lab, rot = rot, cex = cex, col = col)

  ## the annotation
  ggplot2::annotation_custom(xmin=-Inf, ymin=-Inf, xmax=Inf, ymax=Inf,
                             watermarkText)
}



##' Convert 3 RGB Values to Luma
##'
##' The Formula is taken from https://en.wikipedia.org/wiki/Luma_%28video%29
##' @param x numeric vector with 3 values for RGB
##' @return numeric value: the luma of x
##' @author Andreas Leha
##' @export
rgb2luma <- function(x)
{
  0.299 * x[1] + 0.587 * x[2] + 0.114 * x[3]
}

##' Plot a Colour Palette Using ggplot
##'
##' This is inspired by the \code{pal} function defined in the
##'   vignette of the \code{colorspace} package which is available as
##'   \code{andRstuff::pal}.
##'
##' @param v vector of colours
##' @return ggplot
##' @author Andreas Leha
##' @export
##' @examples
##' ggpal(1:10)
ggpal <- function(v) {

  ## the labels
  n <- names(v)
  lab <- v
  if (!is.null(n)) lab <- paste(n, lab, sep="\n")

  ## the luma values
  vRGB <- col2rgb(v)
  vluma <- apply(vRGB, 2, rgb2luma)

  ## assemble a data.frame
  pd <- data.frame(name = (if(is.null(names(v))) NA else names(v)),
                   col  = v,
                   lab  = lab,
                   luma = vluma,
                   num  = as.factor(1:length(v)))

  ## use separate colours vector (easier to handle below...)
  col <- v
  names(col) <- pd$num

  ## set up plot
  p <- ggplot2::ggplot(pd, ggplot2::aes_string(x="num"))

  ## add the colour bars
  p <- p + ggplot2::geom_bar(ggplot2::aes_string(fill="num"), width=1)

  ## add labels to the bars
  p <- p + ggplot2::geom_text(ggplot2::aes_string(y="0.1", label="lab", col="luma<30"), angle=90, hjust=0)

  ## the scales
  p <- p +
    ggplot2::scale_x_discrete(expand=c(0,0)) +
    ggplot2::scale_y_continuous(expand=c(0,0)) +
    ggplot2::scale_fill_manual(guide = FALSE, values=col) +
    ggplot2::scale_colour_grey(guide=FALSE)

  ## theming
  p <- p + ggplot2::theme_minimal()
  p <- p + ggplot2::theme(axis.text  = ggplot2::element_blank(),
                          axis.title = ggplot2::element_blank(),
                          axis.ticks = ggplot2::element_blank(),
                          panel.grid = ggplot2::element_blank())

  ## p <- p + geom_text(aes(y=0.1, label=lab, col=luma), angle=90, hjust=0)
  ## ##p <- p + scale_colour_continuous(low="gray80", high="gray10")
  ## p <- p + scale_colour_gradientn(colours = c("gray90", "gray85", "gray80", "gray85", "gray70", "gray20", "gray15", "gray10"))

  return(p)
}

##' The Stub of a QQ Plot
##'
##' Sets up the plot for a QQ plot.  Leaves points and facets etc. to
##'   the user.  This makes it easy to label / colour points, etc.
##'
##'
##' @param data data.frame containin the data (esp. the p values)
##' @param pvalcol column holding the pvalues to plot
##' @param groupbycols colums to group by.  Might end up in different
##'   colours in the plot, e.g. permutations vs real signal.
##' @param facetvarcols more grouping columns.  Additionally to the
##'   grouped calculations in \code{groupbycols} the confidence
##'   intervals are done grouped by \code{facetvarcols}.
##' @param confidence logical.  if \code{TRUE} (the default)
##'   confidence regions are added to the plot
##' @param confidence.lev numeric.  level for the confidence
##'   intervals.  Defaults to 0.95.
##' @param confidence.col colour of the confidence region.  Defaults
##'   to "gray80"
##' @param confidence.alpha numeric.  transparency of the confidence
##'   region (defaults to 0.5)
##' @param verbose numeric.  Level of verbosity.  If 0 (the default)
##'   no messages and no plots are shown.
##' @return ggplot.
##' @author Andreas Leha
##' @export
##' @examples
##' p <- runif(10000)
##' data <- data.frame(pvalue = p)
##'
##' ## quite empty:
##' ggqq(data)
##'
##' ## add the points:
##' ggqq(data) + geom_point(aes(x = -log10(expected), y = -log10(observed)))
ggqq <- function(data, pvalcol="pvalue", groupbycols=NULL, facetvarcols=NULL, confidence=TRUE, confidence.lev=0.95, confidence.col="gray82", confidence.alpha=0.5, verbose=0)
{

  ## main data for the plot
  pdata <- plyr::ddply(data, c(groupbycols, facetvarcols),
                       function(x) {
                         len <- length(x[[pvalcol]])
                         expected <- ppoints(len)
                         observed <- x[[pvalcol]][order(x[[pvalcol]], decreasing=FALSE)]
                         data.frame(observed=observed,
                                    expected=expected)
                       })

  ## data for the confidence regions
  if (confidence) {
    ## we compute the CI region for each facet separately
    cidata <- plyr::ddply(data, facetvarcols, function(fvdf)
    {
      ## we compute the CI region first for each grouping separately
      fcidata <- plyr::dlply(fvdf, groupbycols, function(grdf)
      {
        len <- nrow(grdf)
        expected <- ppoints(len)
        data.frame(expected = expected,
                   cilow    = qbeta(  confidence.lev, 1:len, len - 1:len + 1),
                   cihigh   = qbeta(1-confidence.lev, 1:len, len - 1:len + 1))
      })

      ## larger groups will have CIs for smaller expected p values
      ## but they will be smaller for larger p values
      ## so, we compute the CIs for the smaller groups first
      ## and use the CIs from the larger groups only for the smaller p
      ## values
      lens <- sapply(fcidata, nrow)
      fcidata <- fcidata[order(lens)]
      fcidata <- fcidata[!duplicated(lens)] ## groups of same length have same CIs

      fcidatad <- fcidata[[1]]
      if (length(fcidata) > 1) {
        for (i in 2:length(fcidata)) {
          fcidatal <- fcidata[[i]]
          fcidatal <- fcidatal[fcidatal$expected < min(fcidatad$expected),]
          fcidatad <- rbind(fcidatad, fcidatal)
        }
      }

      fcidatad
    })
  }

  ## set up the plot
  p <- ggplot2::ggplot(pdata)

  ## add the confidence region
  if (confidence) {
    p <- p + ggplot2::geom_ribbon(ggplot2::aes_string(x    = "-log10(expected)",
                                                      ymin = "-log10(cilow)",
                                                      ymax = "-log10(cihigh)"),
                                  fill  = confidence.col,
                                  alpha = confidence.alpha,
                                  data  = cidata)
  }

  ## add the diagonal
  p <- p + ggplot2::geom_abline(ggplot2::aes(intercept=0, slope=1))

  return(p)
}

##' Draw 'Manhattan' Plots for Human Data
##'
##' This is as simple wrapper around \code{\link{ggmanhattan}}.  It
##'   just gets the chromosome lengths for homo sapiens from the
##'   \code{BSgenome.Hsapiens.UCSC.hg19} package.
##' @param data data.frame.  See \code{\link{ggmanhattan}}.
##' @param verbose numercial.  level of verbosity (0 : no outpout)
##' @param ... additional parameters are passed through to \code{\link{ggmanhattan}}.
##' @return return value of \code{\link{ggmanhattan}}
##' @author Andreas Leha
##' @export
ggmanhattan_hs <- function(data, verbose = 0, ...)
{
  ##library("BSgenome.Hsapiens.UCSC.hg19")

  chr_lengths <- seqlengths(Hsapiens)[paste0("chr", sort(unique(data$CHR)))]
  chrs <- names(chr_lengths)
  chr_lengths <- as.numeric(chr_lengths)
  names(chr_lengths) <- chrs

  ggmanhattan(data, chr_lengths=chr_lengths, verbose=verbose, ...)
}


##' Draw 'Manhattan' Plots using ggplot2
##'
##' This functions draws plots similar to qqman::manhattan.  In an
##'   earlier version that was based on ggplot2, too.  Having the
##'   plots as gplot objects alows for better integration with other
##'   plots.
##' @param data data.frame.  This is expected to contain the columns
##'   \code{CHR}, which should be castable to \code{numeric},
##'   \code{BP} and \code{P}.
##' @param chr_lengths named numeric vector giving the chromosome
##'   lengths.
##' @param hSNPs optional.  Positions to be highlighted.
##' @param verbose numerical. Controlling the level of verbosity (0 : no output).
##' @return gplot object containing the Manhattan plot.
##' @author Andreas Leha
##' @export
ggmanhattan <- function(data,
                        chr_lengths,
                        hSNPs = NULL,
                        verbose = 0)
{
  data$CHR <- as.numeric(data$CHR)
  data$chrCHR <- paste0("chr", data$CHR)

  chrs <- names(chr_lengths)

  chr_offsets <- c(0, cumsum(chr_lengths)[-length(chr_lengths)])
  names(chr_offsets) <- chrs
  chr_centers <- chr_offsets + chr_lengths/2

  data$cumpos <- data$BP + chr_offsets[data$chrCHR]
  data$BP <- as.vector(data$BP)
  data$cumpos <- as.vector(data$cumpos)

  chrs1 <- chrs[seq(1, length(chrs), 2)]
  chrs2 <- chrs[seq(2, length(chrs), 2)]

  ## setup the plot
  p <- ggplot2::ggplot(data, ggplot2::aes_string(x="cumpos", y="-log10(P)"))

  ## add the 'significance' lines
  p <- p + ggplot2::geom_hline(ggplot2::aes_string(yintercept="-log10(1e-05)"), colour="darkblue")
  p <- p + ggplot2::geom_hline(ggplot2::aes_string(yintercept="-log10(5e-08)"), colour="darkred")

  ## add the points
  p <- p + ggplot2::geom_point(colour="gray20", data=data[data$chrCHR %in% chrs1,])
  p <- p + ggplot2::geom_point(colour="gray30", data=data[data$chrCHR %in% chrs2,])

  ## some styling
  p <- p + ggplot2::scale_x_continuous(breaks=chr_centers, labels=names(chr_centers))
  p <- p + ggplot2::theme_bw()
  p <- p + ggplot2::theme(axis.title.x = ggplot2::element_blank(),
                          axis.text.x  = ggplot2::element_text(angle=90, hjust=1, vjust=0.5))


  ## add the highlights
  if (!is.null(hSNPs)) {
    hSNPs <- merge(hSNPs, data)

    p <- p + ggplot2::geom_point(ggplot2::aes_string(colour="as.factor(nfolds)"), data=hSNPs, size=2.3)## +
      ##scale_colour_brewer("# folds", palette=7)
      ##ggplot2::scale_colour_manual("# folds", values=nfoldcols)
    p <- p + ggplot2::theme(legend.position="bottom")
    p <- p + ggplot2::geom_hline(ggplot2::aes_string(yintercept="-log10(1e-05)"), colour="darkblue")
    p <- p + ggplot2::geom_hline(ggplot2::aes_string(yintercept="-log10(5e-08)"), colour="darkred")
    ##p
  }

  return(p)
}


##' Arrange ggplots in a Grid Aligning the Plot Areas
##'
##' This is heavily inspired by \code{cowplot::plot_grid}.
##'   Improvements are: (1) only aligning in each row/col and (2)
##'   trying to to something useful also with facetted plots
##'
##' There are some missing pieces:
##'
##' The alignment could be made more
##'   robust -- see comment in the code.
##'
##' Right now it is necessary to supply both ncol and nrow which could
##' be relaxed.
##'
##' Only ggplot object are allowed, while it would be easy to also
##' allow at least gtables -- see \code{cowplot::plot_grid}.
##'
##' @param ... plots.
##' @param plotlist list of (more) plots.  Will be concatenated with '...'
##' @param ncol integer. number of columns in the arranged plot
##' @param nrow integer. number of rows    in the arranged plot
##' @param align character in c("h", "v", "hv").
##' @param relwidths numeric vector of length(ncol)
##' @param relheights numeric vector of length(nrow)
##' @param bg character. background of the page.
##' @return ggplot
##' @author Andreas Leha, Claus O. Wilke
##' @export
gggrid <- function(..., plotlist=NULL, ncol, nrow, align = "hv", relwidths=NULL, relheights=NULL, bg = "transparent")
{
  plots <- c(list(...), plotlist)

  num_plots <- length(plots)

  idx_plots <- 1:num_plots

  grlayout <- matrix(idx_plots, nrow=nrow, ncol=ncol, byrow=TRUE)

  if (is.null(relwidths))  relwidths  <- rep(1, ncol(grlayout))
  if (is.null(relheights)) relheights <- rep(1, nrow(grlayout))


  grobs <- lapply(plots, function(x) {
    if (!is.null(x)) {
      if (!inherits(x, "grob")) {
        ggplot2::ggplotGrob(x)
      } else
        x
      } else NULL
  })

  halign <- switch(align[1], h = TRUE, vh = TRUE, hv = TRUE,
                   FALSE)
  valign <- switch(align[1], v = TRUE, vh = TRUE, hv = TRUE,
                   FALSE)


  if (valign) {
    for (r in 1:ncol(grlayout)) {
      vidx <- grlayout[,r]
      print(vidx)


      ## a more complete solution would group the plots by nwidths and
      ## first align within each group and only then across groups.

      nwidths <- sapply(grobs[vidx], function(x) length(x$widths))
      if (andRstuff::allSame(nwidths)) {

        ## align all margins
        max_margin_widths <- do.call(grid::unit.pmax, lapply(grobs[vidx],
                                                             function(x) {
                                                               x$widths
                                                             }))

        for (i in vidx) {
          if (!is.null(grobs[[i]])) {
            grobs[[i]]$widths <- max_margin_widths
          }
        }

      } else {

        ## align left margins
        max_lmargin_widths <- do.call(grid::unit.pmax, lapply(grobs[vidx],
                                                              function(x) {
                                                                x$widths[1:3]
                                                              }))

        ## align rigtht margins
        max_rmargin_widths <- do.call(grid::unit.pmax, lapply(grobs[vidx],
                                                              function(x) {
                                                                x$widths[(length(x$widths)-1):length(x$widths)]
                                                              }))


        for (i in vidx) {
          if (!is.null(grobs[[i]])) {
            grobs[[i]]$widths[1:3] <- max_lmargin_widths
            grobs[[i]]$widths[(length(grobs[[i]]$widths)-1):length(grobs[[i]]$widths)] <- max_rmargin_widths
          }
        }
      }
    }
  }

  if (halign) {
    for (r in 1:nrow(grlayout)) {
      hidx <- grlayout[r,]
      print(hidx)

      nheights <- sapply(grobs[hidx], function(x) length(x$heights))
      if (andRstuff::allSame(nheights)) {

        ## align all margins
        max_margin_heights <- do.call(grid::unit.pmax, lapply(grobs[hidx],
                                                             function(x) {
                                                               x$heights
                                                             }))

        for (i in hidx) {
          if (!is.null(grobs[[i]])) {
            grobs[[i]]$heights <- max_margin_heights
          }
        }

      } else {

        ## align top margins
        max_lmargin_heights <- do.call(grid::unit.pmax, lapply(grobs[hidx],
                                                              function(x) {
                                                                x$heights[1:2]
                                                              }))

        ## align bottom margins
        max_rmargin_heights <- do.call(grid::unit.pmax, lapply(grobs[hidx],
                                                              function(x) {
                                                                x$heights[(length(x$heights)-2):length(x$heights)]
                                                              }))


        for (i in hidx) {
          if (!is.null(grobs[[i]])) {
            grobs[[i]]$heights[1:2] <- max_lmargin_heights
            grobs[[i]]$heights[(length(grobs[[i]]$heights)-2):length(grobs[[i]]$heights)] <- max_rmargin_heights
          }
        }
      }
    }
  }

  ggblank(transparentplot=TRUE, bg = bg) + ggplot2::annotation_custom(gridExtra::arrangeGrob(grobs=grobs, nrow = nrow, ncol = ncol, widths = relwidths, heights = relheights))
}

##' Draw a grid 'Plot' on a Fresh Canvas
##'
##' @param p grob to draw
##' @return Nothing.  Called for side effects (plotting) only
##' @author Andreas Leha
##' @export
grid.newpagedraw <- function(p)
{
  grid.newpage()
  grid.draw(p)
}

##' Extract Legend from ggplot Object
##'
##' @param a.gplot a ggplot object
##' @return grop
##' @author Andreas Leha
##' @export
g_legend <- function(a.gplot) {
  tmp <- ggplot2::ggplot_gtable(ggplot2::ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}


##' Create a Completely Empty Plot
##'
##' @param nomargin logical. should the be a margin?
##' @param transparentplot logical.  should the plot be transparent?
##' @param bg character. background of the plot
##' @return ggplot
##' @author Andreas Leha
##' @export
ggblank <- function(nomargin = TRUE, transparentplot=FALSE, bg = "transparent")
{
  dummy <- data.frame(x=1:10, y=1:10)

  p <- ggplot(dummy, aes(x=x, y=y))

  if (!transparentplot) {
    p <- p + geom_blank()
  } else {
    p <- p + geom_point(colour="#FFFFFF00")
  }

  p <- p +
    theme(axis.text        = element_blank(),
          axis.title       = element_blank(),
          axis.ticks       = element_blank(),
          axis.line        = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          rect             = element_rect(fill = bg) # bg of the panel
          ##panel.background = element_blank(),
          ##panel.border     = element_blank(),
          ##plot.background  = element_blank(),
          )

  if (nomargin) {
    p <- p + theme(plot.margin  = unit(c(0,0,0,0), "lines"),
                   panel.spacing = unit(c(0,0,0,0), "lines"))
  }

  return(p)
}


##' combine density and boxplot
##'
##' @param data data.frame
##' @param x character.  giving the column in \code{data} to plot
##' @param grp character.  giving the column in \code{data} to group by
##' @param grpcols vector of colours giving the colours for the levels
##' in data$grp
##' @param shadedgrp character.  giving the column in \code{data} to group by for
##' shaded lines in the density plot
##' @param xlab character.  xlab at the bottom (below the boxplot)
##' @param ylabdens character.  ylab for the top part (the density plot)
##' @param ylabbox character.  ylab for the bottom part (the box plot)
##' @param legendtitle character
##' @param legend.position see \code{\link[ggplot2]{theme}}
##' @param legend.justification see \code{\link[ggplot2]{theme}}
##' @return list of plots
##' named list of class 'hi2single'.  Elements are
##' \item{p_dens}{ggplot.  the density plot on its own}
##' \item{p_boxplots}{ggplot.  the box plot on its own}
##' \item{p_both}{grob.  combined density (top) and box (bottom) plot}
##' \item{p_both_wolegend}{grob.  the same as \code{p_both} but
##' without legend.  This might be handy when this is to be part of a
##' multi-panel figure since the removal of the legend from the grob
##' is not straight forward}
##' @author Andreas Leha
##' @export
##' @examples
##' data("iris")
##'
##' gdbp <- ggdensityboxplot(data=iris, x="Sepal.Length", grp="Species")
##' grid.draw(gdbp$p_both)
##'
##' ## position the legend
##' gdbp <- ggdensityboxplot(data=iris, x="Sepal.Length", grp="Species",
##'                          legend.position = c(1, 1), legend.justification=c(1,1))
##' grid.draw(gdbp$p_both)
ggdensityboxplot <- function(data, x, grp, dohist = FALSE, histbins = 30, dopoints = FALSE, grpcols, shadedgrp, shadedalpha=0.2, solidsize=1, xlab, ylabdens, ylabbox, legendtitle,
                             legend.position="top", legend.justification=c(0,0))
{
  ## -------------------------------- ##
  ## density plot                     ##
  ## -------------------------------- ##

  ## setup
  if (missing(grp)) {
    p_dens <-
      ggplot(data, aes_string(x=x))
  } else {
    p_dens <-
      ggplot(data, aes_string(x=x, colour=grp))
  }

  ## shaded grouping lines
  if (!missing(shadedgrp)) {
    p_dens <- p_dens +
      geom_line(stat="density", aes_string(group=shadedgrp),
                alpha=shadedalpha)
  }

  ## solid grouping lines
  p_dens <- p_dens +
    geom_line(stat="density", size=solidsize)

  ## add histogram
  if (dohist)
    ##p_dens <- p_dens + stat_bin(aes(y = ..density..), bins = histbins, alpha = 0.5, origin = min(data[[x]]))
    p_dens <- p_dens + stat_bin(aes(y = ..density..),
                                breaks = seq(min(data[[x]], na.rm=TRUE),
                                             max(data[[x]], na.rm=TRUE),
                                             length.out=histbins+1),
                                alpha = 0.5)

  ## add points
  if (dopoints)
    p_dens <- p_dens + geom_point(aes(y=0))

  ## colour scale (and legend)
  if (!missing(grp)) {
    if (missing(legendtitle)) legendtitle <- grp
    if (!missing(grpcols)) {
      p_dens <- p_dens + scale_colour_manual(legendtitle, values=grpcols)
    } else {
      p_dens <- p_dens + scale_colour_discrete(legendtitle)
    }
  }

  ## labs
  if (!missing(xlab)) p_dens <- p_dens + xlab(xlab)
  if (!missing(ylabdens)) p_dens <- p_dens + ylab(ylabdens)

  ## theme
  p_dens <- p_dens +
    theme_bw() +
    theme(legend.position=legend.position,
          legend.justification=legend.justification)
  if (!missing(ylabdens) && is.logical(ylabdens) && !ylabdens)
    p_dens <- p_dens +
      theme(axis.title.y=element_blank())


  ## -------------------------------- ##
  ## boxplot                          ##
  ## -------------------------------- ##

  ## setup
  p_boxplots <-
    ggplot(data, aes_string(y=x))
  if (missing(grp)) {
    p_boxplots <- p_boxplots +
      geom_boxplot(aes(x="dummy"),
                   alpha=0.5, notch = FALSE)
  } else {
    p_boxplots <- p_boxplots +
      geom_boxplot(aes_string(x=grp,
                              colour=grp,
                              fill=grp),
                   alpha=0.5, notch = TRUE)
  }



  ## colour scale
  if (!missing(grpcols)) {
    p_boxplots <- p_boxplots + scale_colour_manual(values=grpcols)
    p_boxplots <- p_boxplots + scale_fill_manual(values=grpcols)
  }

  ## rotate
  p_boxplots <- p_boxplots + coord_flip()

  ## labs
  if (!missing(xlab)) p_boxplots <- p_boxplots + ylab(xlab)
  if (!missing(ylabbox) && !is.logical(ylabbox)) p_boxplots <- p_boxplots + xlab(ylabbox)

  ## theme
  p_boxplots <- p_boxplots + theme_bw() +
    theme(legend.position="none")
  if (!missing(ylabbox) && is.logical(ylabbox) && !ylabbox)
    p_boxplots <- p_boxplots + theme(axis.title.y=element_blank())
  if (missing(grp))
    p_boxplots <- p_boxplots + theme(axis.title.y = element_blank(),
                                     axis.text.y  = element_blank(),
                                     axis.ticks.y = element_blank())

  ## -------------------------------- ##
  ## combine                          ##
  ## -------------------------------- ##
  p_both <-
    ggcombine(p_dens + theme(panel.margin=unit(c(0.25, 0.25, 0, 0.25), "lines"),
                             plot.margin=unit(c(1, 1, 0, 0.5), "lines"),
                             ##axis.title.y=element_blank(),
                             axis.ticks.x=element_blank(),
                             axis.title.x=element_blank(),
                             axis.text.x=element_blank()),
              p_boxplots + theme(panel.margin=unit(c(0, 0.25, 0.25, 0.25), "lines"),
                                 plot.margin=unit(c(0, 1, 0.5, 0.5), "lines")),
              direction="vertical",
              heights=c(0.65, 0.35),
              plots=FALSE)

  p_both_wolegend <-
    ggcombine(p_dens + theme(panel.margin=unit(c(0.25, 0.25, 0, 0.25), "lines"),
                             plot.margin=unit(c(1, 1, 0, 0.5), "lines"),
                             ##axis.title.y=element_blank(),
                             axis.ticks.x=element_blank(),
                             axis.title.x=element_blank(),
                             legend.position="none",
                             axis.text.x=element_blank()),
              p_boxplots + theme(panel.margin=unit(c(0, 0.25, 0.25, 0.25), "lines"),
                                 plot.margin=unit(c(0, 1, 0.5, 0.5), "lines")),
              direction="vertical",
              heights=c(0.65, 0.35),
              plots=FALSE)

  return(list(p_dens=p_dens,
              p_boxplots=p_boxplots,
              p_both=p_both,
              p_both_wolegend=p_both_wolegend))
}

##' Helper Function to Position Annotation in Facetted Plots
##'
##' This functions calculates positions for (text) annotations to be
##'   placed on numerically scaled ggplots.  This is especially handy
##'   for facetted plots.
##' @param dataf data.frame used for the (facet of the) plot as well
##' @param x character or numeric giving the column in \code{dataf} to
##'   be mapped to the x axis
##' @param y character or numeric giving the column in \code{dataf} to
##'   be mapped to the y axis
##' @param label character.  the label to position -- will be part of
##'   the returend data.frame
##' @param xpos numeric in [0; 1] giving the position of the label in
##'   units of fractions of plot (or facet) width
##' @param ypos numeric in [0; 1] giving the position of the label in
##'   units of fractions of plot (or facet) height
##' @return data.frame with \code{label}, \code{x} and \code{y}
##'   columns
##' @author Andreas Leha
##' @export
annotationText <- function(dataf, x, y, label, xpos=0.95, ypos=0.05)
{
  x <- as.numeric(dataf[[x]])
  y <- as.numeric(dataf[[y]])

  x <- min(x, na.rm=TRUE) + xpos * (max(x, na.rm=TRUE) - min(x, na.rm=TRUE))
  y <- min(y, na.rm=TRUE) + ypos * (max(y, na.rm=TRUE) - min(y, na.rm=TRUE))

  return(data.frame(label=label,
                    x=x,
                    y=y))
}

##' helper function for ggcorplot
##'
##' generates text labels with statistics to be plotted in ggcorplot
##' @param dataf data.frame holding the data
##' @param x column of dataf
##' @param y column of dataf
##' @param method character.  correlation method.  Possible value in c("pearson," "kendall", "spearman").  See \code{\link[cor]{cor}} for details.  Defaults to "pearson".
##' @param digits_p integer.
##' @param digits_rho integer.
##' @param label_corpval character.  prefix for p value printing
##' @param label_corcoef character.  prefix for correlation coefficient printing
##' @param xpos numeric. where to print the text in measures of plotting area (x coordinate)
##' @param ypos numeric. where to print the text in measures of plotting area (y coordinate)
##' @return data.frame to be used within ggcorplot
##' @author Andreas Leha
##' @export
corAnnotation <- function(dataf, x, y,
                          method = "pearson",
                          digits_p=4, digits_rho=2, label_corpval="p = ", label_corcoef="rho = ",
                          xpos=0.95, ypos=0.05) {
  xv <- as.numeric(dataf[[x]])
  yv <- as.numeric(dataf[[y]])

  idx_x_finite <- is.finite(xv)
  idx_y_finite <- is.finite(yv)
  idx_finite <- idx_x_finite & idx_y_finite
  xv <- xv[idx_finite]
  yv <- yv[idx_finite]

  corcoef <- purrr::possibly(otherwise = NA, .f = cor)(xv, yv, use="complete", method = method)
  corpval <- purrr::possibly(otherwise = list(p.value=NA), .f = cor.test)(xv, yv, method = method)$p.value

  pvaltext <- descutils::prettyPvalues(corpval, digits_p, signiflev=-1, roundonly = FALSE)
  if (grepl("^<", pvaltext)) label_corpval <- sub("=[[:space:]]*$", "", label_corpval)
  label <-
    paste0(label_corcoef, descutils::prettyPvalues(corcoef, digits_rho, roundonly = TRUE),
           "\n",
           label_corpval, pvaltext)

  labelpos <- annotationText(dataf, x, y, label, xpos=xpos, ypos=ypos)

  return(labelpos)
}

##' plot a correlation
##'
##' this plot contains no layers but merely 'sets the scene'
##' @param dataf data.frame containing the variables to correlate
##' @param x column in dataf
##' @param y column in dataf
##' @param xlab character. x axis title
##' @param ylab character. y axis title
##' @param addtext add correlation coefficient, p value etc.
##' @param ... arguments passed to \code{\link{corAnnotation}}
##' @return ggplot object
##' @author Andreas Leha
##' @export
##' @examples
##' ggcorplot(faithful, "eruptions", "waiting") + geom_point() + geom_smooth(method="lm")
ggcorplot <- function(dataf, x, y, xlab, ylab, addtext=TRUE, ...)
{
  xstring <- colnames(dataf[,x, drop=FALSE])
  ystring <- colnames(dataf[,y, drop=FALSE])
  if (missing(xlab)) xlab <- xstring
  if (missing(ylab)) ylab <- ystring

  if (addtext) cortext <- corAnnotation(dataf, x, y, ...)

  xbreaks <- fivenum(dataf[[x]])
  ybreaks <- fivenum(dataf[[y]])
  p <-
    ggplot2::ggplot(dataf,
                    ggplot2::aes_string(x=paste0("`", xstring, "`"),
                                        y=paste0("`", ystring, "`"))) +
    ggplot2::scale_x_continuous(xlab,
                                ##limits=range(dataf$x),
                                breaks=xbreaks,
                                labels=format(xbreaks, scientific=0, digits=2)) +
    ggplot2::scale_y_continuous(ylab,
                                ##limits=range(dataf$y),
                                breaks=ybreaks,
                                labels=format(ybreaks, scientific=0, digits=2)) +
    ##geom_smooth(method="rlm", colour="darkred", fill="gray80") +
    ggplot2::geom_rug(size=0.1) +
    ggplot2::theme_minimal()

  if (addtext) p <- p + ggplot2::annotation_custom(grid::grobTree(grid::textGrob(cortext$label,
                                                                                 x=0.95,
                                                                                 hjust=1,
                                                                                 y=0.05,
                                                                                 vjust=0)))

  return(p)
}


##' Custom Labels for GGally::ggpairs
##'
##' This function enables customization of labels in
##' \code{\link[GGally]{ggpairs}}.
##'
##' Code (GPL-3) taken from \url{http://stackoverflow.com/a/29289613}
##' @param plotObj plot object of class c('gg', 'ggpairs') as produced
##' by \code{\link[GGally]{ggpairs}}
##' @param varLabels character vector of variable labels
##' @param titleLabel character giving the tite
##' @param leftWidthProportion numeric.  set to the same values used
##' in print(plotObj) if you changed these from default
##' @param bottomHeightProportion numeric.  set to the same values used
##' in print(plotObj) if you changed these from default
##' @param spacingProportion numeric.  set to the same values used
##' in print(plotObj) if you changed these from default
##' @param left.opts NULL or list.  If NULL, this default value will be
##' used:
##' \describe{
##'   \item{x}{0}
##'   \item{y}{0.5}
##'   \item{rot}{90}
##'   \item{just}{c('centre', 'centre') where the first gives horizontal justification, second gives vertical}
##'   \item{gp}{list(fontsize=get.gpar('fontsize'))}
##' }
##' @param bottom.opts \code{left.opts}
##' @param title.opts as \code{left.opts} but without the \code{rot} component
##' @return called for side-effects
##' @author Caroline Ring
##' @export
##' @examples
##' if (require(GGally)) {
##'   fake.data <- data.frame(test.1=rnorm(50), #make some fake data for  demonstration
##'                           test.2=rnorm(50),
##'                           test.3=rnorm(50),
##'                           test.4=rnorm(50))
##'
##'   g <- ggpairs(data=fake.data,
##'                columnLabels=rep('', ncol(fake.data)))
##'   #Set columnLabels to a vector of blank column labels
##'   #so that original variable labels will be blank.
##'   print(g)
##'
##'
##'   customize.labels(plotObj=g,
##'                    titleLabel = 'Test plot', #string for title
##'                    left.opts = list(x=-0.5, #moves farther to the left, away from vertical axis
##'                                     y=0.5, #centered with respect to vertical axis
##'                                     just=c('center', 'center'),
##'                                     rot=90,
##'                                     gp=list(col='red',
##'                                             fontface='italic',
##'                                             fontsize=12)),
##'                    bottom.opts = list(x=0.5,
##'                                       y=0,
##'                                       rot=45, #angle the text at 45 degrees
##'                                       just=c('center', 'top'),
##'                                       gp=list(col='red',
##'                                               fontface='bold',
##'                                               fontsize=10)),
##'                    title.opts = list(gp=list(col='green',
##'                                              fontface='bold.italic'))
##'   )
##' }
customize.labels <- function(
                             plotObj,
                             varLabels = NULL,             # vector of variable labels
                             titleLabel = NULL,            # string for title
                             leftWidthProportion = 0.2,    # if you changed these from default...
                             bottomHeightProportion = 0.1, # when calling print(plotObj),...
                             spacingProportion = 0.03,     # then change them the same way here so labels will line up with plot matrix.
                             left.opts = NULL,             # see pattern in left.opts.default
                             bottom.opts = NULL,           # see pattern in bottom.opts.default
                             title.opts = NULL)            # see pattern in title.opts.default
{

  vplayout <- function(x, y) {
    viewport(layout.pos.row = x, layout.pos.col = y)
  }

  numCol <- plotObj$ncol
  if (is.null(varLabels)) {
    varLabels <- colnames(plotObj$data)
    ## default to using the column names of the data
  } else if (length(varLabels) != numCol){
    stop('Length of varLabels must be equal to the number of columns')
  }

  ## set defaults for left margin label style
  left.opts.default <- list(x=0,
                            y=0.5,
                            rot=90,
                            just=c('centre', 'centre'), # first gives horizontal justification, second gives vertical
                            gp=list(fontsize=get.gpar('fontsize')))
  ## set defaults for bottom margin label style
  bottom.opts.default <- list(x=0,
                              y=0.5,
                              rot=0,
                              just=c('centre', 'centre'), # first gives horizontal justification, second gives vertical
                              gp=list(fontsize=get.gpar('fontsize')))
  ## set defaults for title text style
  title.opts.default <- list(x = 0.5,
                             y = 1,
                             just = c(.5,1),
                             gp=list(fontsize=15))

  ## if opts not provided, go with defaults
  if (is.null(left.opts)) {
    left.opts <- left.opts.default
  } else{
    not.given <- names(left.opts.default)[!names(left.opts.default) %in%
                                          names(left.opts)]
    if (length(not.given)>0){
      left.opts[not.given] <- left.opts.default[not.given]
    }
  }

  if (is.null(bottom.opts)) {
    bottom.opts <- bottom.opts.default
  } else{
    not.given <- names(bottom.opts.default)[!names(bottom.opts.default) %in%
                                            names(bottom.opts)]
    if (length(not.given)>0){
      bottom.opts[not.given] <- bottom.opts.default[not.given]
    }
  }

  if (is.null(title.opts)) {
    title.opts <- title.opts.default
  } else{
    not.given <- names(title.opts.default)[!names(title.opts.default) %in%
                                           names(title.opts)]
    if (length(not.given)>0){
      title.opts[not.given] <- title.opts.default[not.given]
    }
  }

  showLabels <- TRUE
  viewPortWidths <- c(leftWidthProportion,
                      1,
                      rep(c(spacingProportion,1),
                          numCol - 1))
  viewPortHeights <- c(rep(c(1,
                             spacingProportion),
                           numCol - 1),
                       1,
                       bottomHeightProportion)

  viewPortCount <- length(viewPortWidths)

  if(!is.null(titleLabel)){
    pushViewport(viewport(height = unit(1,"npc") - unit(.4,"lines")))
    do.call('grid.text', c(title.opts[names(title.opts)!='gp'],
                           list(label=titleLabel,
                                gp=do.call('gpar',
                                           title.opts[['gp']]))))
    popViewport()
  }

  ## viewport for Left Names
  pushViewport(viewport(width=unit(1, "npc") - unit(2,"lines"),
                        height=unit(1, "npc") - unit(3, "lines")))

  ## new for axis spacingProportion
  pushViewport(viewport(layout = grid.layout(
                          viewPortCount, viewPortCount,
                          widths = viewPortWidths, heights = viewPortHeights
                        )))

  ## Left Side
  for(i in 1:numCol){
    do.call('grid.text',
            c(left.opts[names(left.opts)!='gp'],
              list(label=varLabels[i],
                   vp = vplayout(as.numeric(i) * 2 - 1 ,1),
                   gp=do.call('gpar',
                              left.opts[['gp']]))))
  }
  popViewport() # layout
  popViewport() # spacing

  ## viewport for Bottom Names
  pushViewport(viewport(width=unit(1, "npc") - unit(3,"lines"),
                        height=unit(1, "npc") - unit(2, "lines")))

  ## new for axis spacing
  pushViewport(viewport(layout = grid.layout(
                          viewPortCount, viewPortCount,
                          widths = viewPortWidths, heights = viewPortHeights)))

  ## Bottom Side
  for(i in 1:numCol){
    do.call('grid.text',
            c(bottom.opts[names(bottom.opts)!='gp'],
              list(label=varLabels[i],
                   vp = vplayout(2*numCol, 2*i),
                   gp=do.call('gpar',
                              bottom.opts[['gp']]))))
  }

  popViewport() # layout
  popViewport() # spacing
}

openBlankDev <- function(filename) {
  create_new_dev <- TRUE

  ## what is the current device?
  devcur <- dev.cur()

  if (names(devcur) == "null device")
    create_new_dev <- FALSE

  ## create a tempfile name if none is passed
  if (missing(filename)) filename <- tempfile()

  ## save the current parameters
  oldpar <- par()

  ## if no current device, open new one
  if (!create_new_dev) {
    newdev <- dev.cur()
  } else {
    newdev <-
      switch(names(devcur),
             png={devsize <- dev.size("px")
                  png(filename=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             pdf={devsize <- dev.size("in")
                  pdf(file=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             postscript={devsize <- dev.size("in")
                  postscript(file=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             xfig={devsize <- dev.size("in")
                  xfig(file=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             X11cairo={devsize <- dev.size("in")
                  X11(type="cairo", width=devsize[1], height=devsize[2])
                  dev.cur()},
             X11={devsize <- dev.size("in")
                  X11(type="Xlib", width=devsize[1], height=devsize[2])
                  dev.cur()},
             cairo_pdf={devsize <- dev.size("in")
                  cairo_pdf(filename=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             cairo_ps={devsize <- dev.size("in")
                  cairo_ps(filename=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             svg={devsize <- dev.size("in")
                  svg(filename=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             jpeg={devsize <- dev.size("px")
                   jpeg(filename=filename, width=devsize[1], height=devsize[2])
                   dev.cur()},
             bmp={devsize <- dev.size("px")
                  bmp(filename=filename, width=devsize[1], height=devsize[2])
                  dev.cur()},
             tiff={devsize <- dev.size("px")
                   tiff(filename=filename, width=devsize[1], height=devsize[2])
                   dev.cur()},
             "tikz output"={devsize <- dev.size("in")
                          tikz(file=filename, width=devsize[1], height=devsize[2])
                          dev.cur()},
             stop("unsupported graphics device"))
  }

  ## reset to old parameters
  par(oldpar[!names(oldpar) %in% c("cin", "cra", "csi", "cxy", "din")])

  ## return the new device
  newdev
}

##' Create a Kaplan-Meier plot using ggplot2
##'
##' Kaplan-Meier plots using ggplot2.  Extras: numbers at risk below
##' the main panel.  Legend inside the main panel.
##' @param sfit a \code{\link[survival]{survfit}} object
##' @param table logical: Create a table graphic below the K-M plot, indicating at-risk numbers?
##' @param returns logical: if \code{TRUE}, return an arrangeGrob object
##' @param plots logical: if \code{TRUE}, plot sth
##' @param xlims
##' @param xlabs x-axis label
##' @param ylabs y-axis label
##' @param ylabvjust numerical.  y-axis lable positioning.  Defaults to 0.1
##' @param plot_events boolean.  should the events be marked by '+'?
##' @param ystratalabs The strata labels. \code{Default = levels(summary(sfit)$strata)}
##' @param ystrataname The legend name. Default = "Strata"
##' @param ystratacols The strata cols. \code{Default = "black"}
##' @param ystrataltys
##' @param manual_strata Vector.  If given this is used as strata "classification" overriding \code{summary(sfit, extend=T)$strata}
##' @param stripstrataprefix
##' @param legend.position
##' @param legend.justification
##' @param timeby numeric: control the granularity along the time-axis
##' @param timebystart numeric: where to start the xticks.  When NULL it is set to min(0, min(times)).  Defaults to 0.
##' @param main plot title
##' @param pval logical: add the pvalue to the plot?
##' @param pval_freetext character.  if given has precedence over any
##' calculated pvalue.  Will be shown instead of the p value.
##' @param pval_from character.  If "logrank" or "survdiff"
##' \code{survdiff} is used to calculate the pvalue.  If "lrt" or
##' "coxph" \code{coxph} is used to calculate the pvalue (from the
##' likelihood ratio test).
##' @param pval_value numeric.  if not NULL, print the provided value as
##' p value.  If NULL, calc the p value from the log rank test.
##' Defaults to NULL
##' @param pval_digits numeric. the number of digits to print for the p value
##' @param pval.position
##' @param hr logical. add the hazard ratio to the plot?
##' @param hr_ci logical. add the 95\% confidence interval for the hazard ratio to the plot?
##' @param ggkmS Surv object.  the survival object sfit was trained on (needed to calc pval correctly)
##' @param svars character vector.  the predictors used in the sfit (needed to calc pval correctly)
##' @param sdat data.frame.  holding the \code{svars} (needed to calc pval correctly)
##' @param table_strata_factor numeric.  a scale factor for the rows of the 'numbers at risk' table.  Defaults to 0.1
##' @param ... not used
##' @return a ggplot is made. if returns=TRUE, then an arrangeGlob object
##' is returned
##' @author Abhijit Dasgupta with contributions by Gil Tomas and extensions by Andreas Leha
##' \url{http://statbandit.wordpress.com/2011/03/08/an-enhanced-kaplan-meier-plot/}
##' @name ggkm
##' @export
##' @examples
##' \dontrun{
##' data(colon)
##' fit <- survfit(Surv(time,status)~rx, data=colon)
##' ggkm(fit, timeby=500)
##' }
ggkm <- function(sfit,
                 table = TRUE,
                 returns = FALSE,
                 plots = TRUE,
                 xlims = NULL,
                 xlabs = "Time",
                 ylabs = "Survival Probability",
                 ylabvjust = 0.1,
                 plot_events = TRUE,
                 ystratalabs = NULL,
                 ystrataname = NULL,
                 ystratacols = NULL,
                 ystrataltys = NULL,
                 manual_strata=NULL,
                 stripstrataprefix = FALSE,
                 legend.position = NULL,
                 legend.justification = NULL,
                 timeby = 100,
                 timebystart = 0,
                 main = "Kaplan-Meier Plot",
                 pval = TRUE,
                 pval_freetext = NULL,
                 pval_from = "logrank",
                 pval_value = NULL,
                 pval_digits = 2,
                 pval.position = NULL,
                 hr = TRUE,
                 hr_ci = TRUE,
                 ggkmS = NULL,
                 svars=NULL,
                 sdat = NULL,
                 table_strata_factor=0.1, ...) {

  if(is.null(ystratalabs)) {
    ##ystratalabs <- as.character(levels(summary(sfit)$strata))
    ystratalabs <- names(sfit$strata)
  }

  if (!is.null(manual_strata)) {
    naS <- rep(FALSE, nrow(ggkmS))
    dupS <- rep(FALSE, nrow(ggkmS))
    if (sum(is.na(ggkmS)) > 0) {
      warning("NA entries in Surv object ggkmS.  These are omitted")
      naS <- is.na(ggkmS)
    }
    if (sum(duplicated(as.factor(paste(ggkmS[,1],ggkmS[,2])))) > 0) {
      warning("duplicate entries in the Surv object ggkmS.  Using only unique ones")
      dupS <- duplicated(as.factor(paste(ggkmS[,1], ggkmS[,2])))
    }
    manual_strata <- manual_strata[(!naS) & (!dupS)]
    if (!all(sort(manual_strata)==manual_strata)) {
      warning("sorting the manual_strata.  I hope the sorting is consistent with the one from the survival functions")
      manual_strata <- sort(manual_strata)
    }
    if (is.numeric(manual_strata)) {
      manual_strata <- ystratalabs[manual_strata]
    }
  } else {
    if (length(ystratalabs) > length(names(sfit$strata))) {
      warning("more ystratalabs given than strata present.  I am assuming the first ystratalabs to contain values.\n\nYou might need to check the order of the ystratalabs.")
    }
  }

  if(is.null(ystratacols)) {
    ystratacols <- rep("black",
                       length.out=max(length(sfit$strata),
                         length(ystratalabs)))
  } else {
    ystratacols <- rep(ystratacols,
                       length.out=max(length(sfit$strata),
                         length(ystratalabs)))
  }

  if (is.null(ystrataltys)) {
    ystrataltys <- seq_along(ystratacols)
  } else { ## recycle if necessary
    ystrataltys <- rep(ystrataltys, length.out = length(ystratacols))
  }

  m <- max(nchar(ystratalabs))

  if(is.null(ystrataname)) ystrataname <- "Strata"

  if (is.null(timebystart)) {
    times <- seq(min(0, sfit$time), max(sfit$time), by = timeby)
  } else {
    times <- seq(timebystart, max(sfit$time), by = timeby)
  }

  .df <-
    data.frame(time = sfit$time,
               n.risk = sfit$n.risk,
               n.event = sfit$n.event,
               surv = sfit$surv,
               strata = {if (!is.null(manual_strata)) manual_strata else summary(sfit, censored = T)$strata},
               upper = sfit$upper,
               lower = sfit$lower)

  if (all(is.na(factor(as.character(.df$strata),
                       levels=ystratalabs)))) {
    .df$strata <- factor(ystratalabs[as.numeric(.df$strata)],
                         levels=ystratalabs)
  } else {
    .df$strata <- factor(as.character(.df$strata),
                         levels=ystratalabs)
  }

  if (length(unique(.df$strata)) > 1) {
    zeros <-
      data.frame(time = min(0, sfit$time),
                 surv = 1,
                 strata = {if (min(sfit$time) > 0) factor(names(table(.df$strata))[table(.df$strata) > 0], levels=levels(.df$strata)) else factor(setdiff(names(table(.df$strata))[table(.df$strata) > 0],
                                                                                                             .df[order(.df$time),"strata"][1]),
                                                                                                             levels=levels(.df$strata))},
                 upper = 1,
                 lower = 1)
    .df <- rbind.fill(zeros, .df)
  }

  ## add 'n'
  stratan <- sfit$n
  ##names(stratan) <- names(sfit$strata)
  names(stratan) <- ystratalabs
  levels(.df$strata) <- paste0(levels(.df$strata), " (n=", stratan[as.character(levels(.df$strata))], ")")

  if (stripstrataprefix)
    levels(.df$strata) <- gsub("^[^=]*=", "", levels(.df$strata))

  ## nstrata
  d <- length(levels(.df$strata))

  ## calc legend.position if not given
  if (is.null(legend.position)) {
    legend.position = c(ifelse(m < 10, .28, .35),
                        ifelse(d < 4, .25, .35))
  }

  ##if (!is.null(xlims)) xlimits <- xlims else xlimits <- c(min(0, sfit$time),
  ##                           max(sfit$time))
  p <- ggplot(.df[c(1:nrow(.df),
                    which(.df$strata == names(table(.df$strata)[table(.df$strata) == 1]))),],
              aes_string(x="time", y="surv")) +
    geom_step(aes_string(linetype = "strata", colour = "strata"),
              size = 0.7) +
    theme_bw() +
    scale_x_continuous(xlabs,
                       breaks = round(times)) +
    scale_y_continuous(ylabs, limits = c(0, 1)) +
    scale_colour_manual(name="Strata", values=ystratacols) +
    scale_linetype_manual(name="Strata", values = ystrataltys) +
    theme(axis.title = element_text(),
          axis.title.x = element_text(vjust = 0.5),
          axis.title.y = element_text(angle = 90, vjust = ylabvjust),
          panel.grid.minor = element_blank(),
          legend.position = legend.position,
          legend.key = element_rect(colour = NA))

  if (!is.null(legend.justification))
    p <- p + theme(legend.justification = legend.justification)

  if (!is.null(xlims))
    p <- p + coord_cartesian(xlim = xlims)

  if (!is.null(main) && !identical(pval.position, "title"))
    p <- p + ggtitle(main)


  if (plot_events)
    p <- p + geom_point(aes_string(colour="strata"), pch="+", size=5)


  ## Create a blank plot for place-holding
  ## .df <- data.frame()
  blank.pic <- ggplot(.df, aes_string(x="time", y="surv")) +
    geom_blank() +
      theme_bw() +
        theme(axis.text.x = element_blank(),
              axis.text.y = element_blank(),
              axis.title.x = element_blank(),
              axis.title.y = element_blank(),
              axis.ticks = element_blank(),
              panel.grid.major = element_blank(),
              panel.grid.minor = element_blank(),
              panel.border = element_blank())

  pvaltxt <- NULL
  if (pval) {
    if (is.null(pval_value)) {
      if (is.null(summary(sfit, times=times, extend = TRUE)$strata)) {
        pval <- NULL
        warning("Only one group - not computing a p value")
      } else {
        if (is.null(svars) || is.null(ggkmS) || is.null(sdat)) {
          warning("You did not supply all the data needed to calc the p value.\nTherefore, I am getting the data from the call in the sfit-object.\n\nThis won't work inside llply, for instance!")
          ## -------------------------
          ## 2 possibilities to call survdiff here:
          ##
          ## (1)
          ## works, but produces slightly bigger p-values (not sure yet why, but may be due to n.all>n)
          ##stratname <- unique(unlist(lapply(strsplit(names(sfit$strata), "="), function(x) x[1])))
          ##stratvals <- unlist(lapply(strsplit(names(sfit$strata), "="), function(x) x[2]))
          ##stratvals <- rep(stratvals, sfit$strata)
          ##stratdata <- data.frame(tmpstrat=stratvals)
          ##names(stratdata) <- stratname
          ##sdiffS <- survival::Surv(sfit$time, sfit$n.event)
          ##sdiff <- survdiff(reformulate(stratname, response="sdiffS"), data=stratdata)
          ## (2)
          ## fails inside llply, but produces correct p-values
          if (pval_from %in% c("coxph", "lrt")) {
            sdiff <- survival::coxph(eval(sfit$call$formula),
                                     data = eval(sfit$call$data))
          } else if (pval_from %in% c("survdiff", "logrank")) {
            sdiff <- survdiff(eval(sfit$call$formula),
                              data = eval(sfit$call$data))
          }
        ## -------------------------
      } else {
          if (pval_from %in% c("coxph", "lrt")) {
            sdiff <- survival::coxph(reformulate(svars, response="ggkmS"),
                                     data=sdat)
          } else if (pval_from %in% c("survdiff", "logrank")) {
            sdiff <- survdiff(reformulate(svars, response="ggkmS"),
                              data=sdat)
          }
        }
        ## if used with survdiff
        if (pval_from %in% c("coxph", "lrt")) {
          pval <- summary(sdiff)$logtest['pvalue']
        } else if (pval_from %in% c("survdiff", "logrank")) {
          pval <- pchisq(sdiff$chisq,
                         length(sdiff$n) - 1,
                         lower.tail = FALSE)
        }
      }
    } else {
      pval <- pval_value
    }
    if (!is.null(pval)) {
      pval_formatted <- descutils::prettyPvalues(pval, digits=pval_digits, signiflev = 0)
      pvaltxt <- ifelse(grepl("^<", pval_formatted),
                        paste("p", pval_formatted),
                        paste("p =", pval_formatted))
    }
  }
  if (!is.null(pval_freetext))
    pvaltxt <- pval_freetext
  if (!is.null(pvaltxt)) {
    if (is.null(pval.position)) {
        p <- p + annotate("text",
                          x = max(sfit$time),
                          y = 0.1,
                          hjust = 1,
                          size = 4,
                          label = pvaltxt)
      } else if (identical(pval.position, "title")) {
        p <- p + ggtitle(pvaltxt)
      } else {
        p <- p + annotate("text",
                          x = pval.position[1],
                          y = pval.position[2],
                          hjust = 1,
                          size = 4,
                          label = pvaltxt)
      }
  }

  if(hr) {
    if (is.null(summary(sfit, times=times, extend = TRUE)$strata)) {
      warning("Only one group - not computing a hazard ratio")
    } else {
      if (is.null(svars) | is.null(ggkmS) | is.null(sdat)) {
        warning("You did not supply all the data needed to calc the coxph.\nTherefore, I am getting the data from the call in the sfit-object.\n\nThis won't work inside llply, for instance!")
        coxfit <- survival::coxph(eval(sfit$call$formula),
                        data = eval(sfit$call$data))
        ## -------------------------
      } else {
        coxfit <- survival::coxph(reformulate(svars, response="ggkmS"),
                        data=sdat)
      }
      sumcoxfit <- summary(coxfit)
      coxfitcoef <- sumcoxfit$conf.int
      coxfitcoef_formatted <- format(coxfitcoef, digits=1, nsmall=2)
      hratio <- coxfitcoef_formatted[1]
      hratio_l <- coxfitcoef_formatted[3]
      hratio_u <- coxfitcoef_formatted[4]
      p <- p + annotate("text",
                        x = max(sfit$time),
                        y = 0.03,
                        hjust = 1,
                        size = 4,
                        label = paste0("HR = ", hratio, ", 95%-CI: [", hratio_l, "; ", hratio_u, "]"))
    }
  }

  if(table) {
    ## Create table graphic to include at-risk numbers
    risk.data <-
      data.frame(
        strata={if (is.null(summary(sfit,
                                    times = times,
                                    extend = TRUE)$strata))
                  unique(.df$strata) else summary(sfit,
                                                  times = times,
                                                  extend = TRUE)$strata},
        time = summary(sfit, times = times, extend = TRUE)$time,
        n.risk = summary(sfit, times = times, extend = TRUE)$n.risk,
        n.risk.formatted = format(summary(sfit,
          times = times,
          extend = TRUE)$n.risk,
          nsmall=0))
    nstrata <- length(unique(risk.data$strata))
    if (nstrata > 1) {
      if (!all(table(risk.data$time) == nstrata)) {
        times_w_missing_rows <- as.numeric(names(table(risk.data$time)[table(risk.data$time) < nstrata]))
        for (time_w_missing_rows in times_w_missing_rows) {
          row_w_missing_rows <- risk.data[abs(risk.data$time-times_w_missing_rows) < timeby/2,]
          strata_w_missing_rows <- setdiff(unique(risk.data$strata), row_w_missing_rows$strata)
          for (mstrat in strata_w_missing_rows) {
            mstrat_risk.data <- risk.data[risk.data$strata==mstrat,]
            mstrat_missing_row <- mstrat_risk.data[order(mstrat_risk.data$time - time_w_missing_rows)[1], ]
            mstrat_missing_row$time <- time_w_missing_rows
            risk.data <- rbind(risk.data, mstrat_missing_row)
          }
        }
      }
    }
    levels(risk.data$strata) <- ystratalabs
    risk.data$strata <- factor(risk.data$strata, levels=rev(ystratalabs))
    if (stripstrataprefix)
      levels(risk.data$strata) <- gsub("^[^=]*=", "", levels(risk.data$strata))

    data.table <-
      ggplot(risk.data, aes_string(x = "time",
                                   y = "strata",
                                   label = "n.risk.formatted")) +
                                     ##, color = strata)) +
        geom_text(size = 3.5) +
          theme_bw() +
            scale_y_discrete(breaks = as.character(levels(risk.data$strata))) +
            ## scale_y_discrete(#format1ter = abbreviate,
            ## breaks = 1:3,
            ## labels = ystratalabs) +
            scale_x_continuous("Numbers At Risk",
                               limits = range(c(0,sfit$time))) +
             theme(axis.title.x = element_text(size = 10, vjust = 1),
                   panel.grid.minor = element_blank(),
                   panel.grid.major = element_blank(),
                   ## panel.border = element_blank(),
                   ##panel.border = element_rect(colour = "grey50"),
                   axis.text.x = element_blank(),
                   axis.ticks = element_blank(),
                   axis.text.y = element_text(face = "bold", hjust = 1))
    data.table <- data.table + theme(legend.position = "none") +
      xlab(NULL) + ylab(NULL)

    ## Plotting the graphs
    ## p <- ggplotGrob(p)
    ## p <- addGrob(p, textGrob(x = unit(.8, "npc"), y = unit(.25, "npc"), label = pvaltxt,
    ## gp = gpar(fontsize = 12)))
    gp <- ggplot2::ggplot_gtable(ggplot2::ggplot_build(p))
    gdata.table <- ggplot2::ggplot_gtable(ggplot2::ggplot_build(data.table))
    maxWidth = grid::unit.pmax(gp$widths[2:3], gdata.table$widths[2:3])
    gp$widths[2:3] <- as.list(maxWidth)
    gdata.table$widths[2:3] <- as.list(maxWidth)
    a <- arrangeGrob(gp, gdata.table,
                     clip = FALSE,
                     ncol = 1,
                     heights = unit(c(2, .2+nstrata*table_strata_factor),
                       c("null", "null")))
    if (plots) grid.draw(a)
    if (returns) return(a)
  }
  else {
    ## p <- ggplotGrob(p)
    ## p <- addGrob(p, textGrob(x = unit(0.5, "npc"), y = unit(0.23, "npc"),
    ## label = pvaltxt, gp = gpar(fontsize = 12)))
    if (plots) grid.draw(p)
    if(returns) return(p)
  }
}

##' simple heatmap plotting with ggplot
##'
##' Plots a numerical matrix as heatmap.  Very simple - no dendrograms,
##' no clustering, no histogram, no annotation
##' @param mat matrix to plot
##' @param names axis names
##' @param ltitle legend title
##' @param orderx optional.  order of columns
##' @param ordery optional.  order of rows
##' @param gradient_opts list. optional.  passed to scale_fill_gradient()
##' @param theme_opts list. optional. passed to theme()
##' @return ggplot2 plot
##' @author learnr \email{rlearnr@@gmail.com} with modifications by Andreas Leha
##' \url{https://learnr.wordpress.com/2010/01/26/ggplot2-quick-heatmap-plotting/}
##' @export
##' @examples
##' ggheatmap(matrix(rnorm(50*10), 50, 10))
ggheatmap <- function(mat,
                      names=c("X", "Y"),
                      ltitle="value",
                      orderx,
                      ordery,
                      gradient_opts=NULL,
                      theme_opts=NULL) {
  origrownames <- rownames(mat)
  origcolnames <- colnames(mat)

  if (is.null(origrownames)) origrownames <- paste0("r", 1:nrow(mat))
  if (is.null(origcolnames)) origcolnames <- paste0("r", 1:ncol(mat))

  rownames(mat) <- paste0("r", 1:nrow(mat))
  colnames(mat) <- paste0("c", 1:ncol(mat))

  melt_mat <- cbind(rownames(mat), as.data.frame(mat))
  ## we need long format for ggplot
  pl_mat <- reshape::melt.data.frame(melt_mat)
  ##pl_mat <- pl_mat[,c(2,1,3)]
  names(pl_mat) <- c(names, "value")

  ## make sure, that the x and y values are characters (not numbers)
  pl_mat[,1] <- as.character(pl_mat[,1])
  pl_mat[,2] <- as.character(pl_mat[,2])

  if (missing(orderx)) {
    orderx <- 1:ncol(mat)
  }
  if (missing(ordery)) {
    ordery <- 1:nrow(mat)
  }
  ## reverse the y order
  ordery <- rev(ordery)
  pl_mat[,2] <- factor(pl_mat[,2], levels=colnames(mat)[orderx])
  pl_mat[,1] <- factor(pl_mat[,1], levels=rownames(mat)[ordery])

  ## the plot
  gradargs <- list(name=ltitle,
                   low = "white",
                   high = "steelblue",
                   na.value="red"
                   ##trans='log',
                   ##labels = percent_format()
                   )
  if (!is.null(gradient_opts)) {
    gradargs <- c(gradient_opts, gradargs)
    gradargs <- gradargs[!duplicated(names(gradargs))]
  }
  p <- ggplot(pl_mat, aes_string(x=names[2], y=names[1])) +
    geom_tile(aes_string(fill="value")) +
        do.call(scale_fill_gradient, gradargs)

  base_size <- 9
  p <-
      p +
          theme_gray(base_size = base_size) +
              labs(x=names[2], y=names[1]) +
                  scale_x_discrete(expand = c(0, 0), labels=origcolnames[orderx], drop=FALSE) +
                      scale_y_discrete(expand = c(0, 0), labels=origrownames[ordery],drop=FALSE) +
                          ##coord_fixed() +
                          theme(legend.position = "none",
                                rect=element_rect(colour="white",fill="white"),
                                axis.ticks = element_blank(),
                                axis.text.x = element_text(size = base_size * 0.8,
                                    angle = 90,
                                    hjust = 0,
                                    ##vjust = 0,
                                    colour = "grey50")##,
                                ##axis.text.y = element_blank()##,
                                ##axis.title.x = element_text(size=base_size*1.5),
                                ##axis.title.y = element_text(size=base_size*1.5,
                                ##  angle=90)
                                )
  if (!is.null(theme_opts))
      p <- p + do.call(theme, theme_opts)

  p
}


##' Plotting of prediction-error-curves
##'
##' Plots prediction error curves.
##' @title ggplot_pec
##' @param pecobj pec object resulting from \code{\link[pec]{pec}}
##' @param errtoplot character.  Either "AppErr" (default) for the
##'   training error.  Or "crossvalErr" for the test error estimation
##'   of the cross validation
##' @return ggplot object
##' @author Andreas Leha
##' @export
##' @examples
##' if (require(pec) && require(prodlim) && require(survival)) {
##'   set.seed(130971)
##'   ## simulate an artificial data frame
##'   ## with survival response and two predictors
##'
##'   dat <- SimSurv(300)
##'
##'   ## fit some candidate Cox models and compute the Kaplan-Meier estimate
##'
##'   Models <- list("Cox.X1"=coxph(Surv(time,status)~X1,data=dat,y=TRUE),
##'                  "Cox.X2"=coxph(Surv(time,status)~X2,data=dat,y=TRUE),
##'                  "Cox.X1.X2"=coxph(Surv(time,status)~X1+X2,data=dat,y=TRUE))
##'
##'   ## compute the apparent prediction error
##'   PredError <- pec(object=Models,
##'                    formula=Surv(time,status)~X1+X2,
##'                    data=dat,
##'                    exact=TRUE,
##'                    cens.model="marginal",
##'                    splitMethod="none",
##'                    B=0,
##'                    verbose=TRUE)
##'
##'   ggplot_pec(PredError) + xlim(c(0,30))
##' }
ggplot_pec <- function(pecobj, errtoplot = "AppErr") {
  ggg <- data.frame(time=pecobj$time,
                    as.data.frame(pecobj[[errtoplot]]))
  ggg <- reshape(ggg,
                 direction="long",
                 varying=list(names(ggg)[-1]),
                 v.names="brierscore",
                 idvar=c("time"),
                 timevar="model",
                 times=names(pecobj[[errtoplot]]))
  ggplot(ggg, aes_string(x="time", y="brierscore", colour="model")) +
    geom_line()
}



##' generating data to draw ROC curves
##'
##' helper function to create the data needed for the ROC curve plotting
##' @title rocdata
##' @param grp labels classifying subject status
##' @param pred values of each observation
##' @return List with 2 components
##' @author Kate Nambiar \email{k.z.nambiar@@bsms.ac.uk} with modifications by Andreas Leha
##' \url{http://www.rrandomness.com/r/simple-roc-plots-with-ggplot2-part-1/}
##' @export
##' @keywords internal
rocdata <- function(grp, value) {
  grp <- factor(grp, ordered=TRUE)

  dropped <- is.na(grp) | is.na(value)
  if (any(dropped)) {
    warning(paste("dropped", sum(dropped), "samples due to NAs"))
    grp <- grp[!dropped]
    value <- value[!dropped]
  }

  tp <- sapply(value, function(x) length(which(value > x & grp == levels(grp)[2])))
  fn <- sapply(value, function(x) length(which(value < x & grp == levels(grp)[2])))
  fp <- sapply(value, function(x) length(which(value > x & grp == levels(grp)[1])))
  tn <- sapply(value, function(x) length(which(value < x & grp == levels(grp)[1])))

  tpr <- tp / (tp + fn)
  fpr <- fp / (fp + tn)

  youden <- (1 - tpr)^2 + (1 - (1-fpr))^2# - 1
  youden.idx <- which(youden == min(youden))[1]
  youden.fpr <- fpr[youden.idx]
  youden.tpr <- tpr[youden.idx]
  youden.idx <- value[youden.idx]

  roc <- data.frame(x = fpr, y = tpr)
  roc <- roc[order(roc$x, roc$y),]

  i <- 2:nrow(roc)
  auc <- (roc$x[i] - roc$x[i - 1]) %*% (roc$y[i] + roc$y[i - 1])/2

  pos <- value[grp == levels(grp)[2]]
  neg <- value[grp == levels(grp)[1]]
  ##auc.wilcox <- wilcox.test(pos,neg,exact=0)$statistic /(length(pos)*length(neg))

  q1 <- auc/(2-auc)
  q2 <- (2*auc^2)/(1+auc)
  se.auc <- sqrt(((auc * (1 - auc)) + ((length(pos) -1)*(q1 - auc^2)) + ((length(neg) -1)*(q2 - auc^2)))/(length(pos)*length(neg)))
  ci.upper <- auc + (se.auc * 0.96)
  ci.lower <- auc - (se.auc * 0.96)

  se.auc.null <- sqrt((1 + length(pos) + length(neg))/(12*length(pos)*length(neg)))
  z <- (auc - 0.5)/se.auc.null
  p <- 2*pnorm(-abs(z))

  stats <- data.frame (auc = auc,
                       p.value = p,
                       ci.upper = ci.upper,
                       ci.lower = ci.lower
                       )

  return (list(roc=roc,
               stats=stats,
               dropped=dropped,
               youden=list(cutoff=youden.idx,
                 fpr=youden.fpr,
                 tpr=youden.tpr)))
}

##' Produces x and y co-ordinates for ROC curve plot
##'
##' Produces x and y co-ordinates for ROC curve plot.  Original version from the web.  Helper function.
##' @title orig_rocdata
##' @param grp labels classifying subject status
##' @param pred values of each observation
##' @return List with 2 components
##' @author Kate Nambiar \email{k.z.nambiar@@bsms.ac.uk} with modifications by Andreas Leha
##' \url{http://www.rrandomness.com/r/simple-roc-plots-with-ggplot2-part-1/}
orig_rocdata <- function(grp, pred) {

  grp <- as.factor(grp)
  if (length(pred) != length(grp)) {
    stop("The number of classifiers must match the number of data points")
  }

  if (length(levels(grp)) != 2) {
    stop("There must only be 2 values for the classifier")
  }

  cut <- unique(pred)
  tp <- sapply(cut, function(x) length(which(pred > x & grp == levels(grp)[2])))
  fn <- sapply(cut, function(x) length(which(pred < x & grp == levels(grp)[2])))
  fp <- sapply(cut, function(x) length(which(pred > x & grp == levels(grp)[1])))
  tn <- sapply(cut, function(x) length(which(pred < x & grp == levels(grp)[1])))
  tpr <- tp / (tp + fn)
  fpr <- fp / (fp + tn)
  roc = data.frame(x = fpr, y = tpr)
  roc <- roc[order(roc$x, roc$y),]

  i <- 2:nrow(roc)
  auc <- (roc$x[i] - roc$x[i - 1]) %*% (roc$y[i] + roc$y[i - 1])/2

  pos <- pred[grp == levels(grp)[2]]
  neg <- pred[grp == levels(grp)[1]]
  q1 <- auc/(2-auc)
  q2 <- (2*auc^2)/(1+auc)
  se.auc <- sqrt(((auc * (1 - auc)) + ((length(pos) -1)*(q1 - auc^2)) + ((length(neg) -1)*(q2 - auc^2)))/(length(pos)*length(neg)))
  ci.upper <- auc + (se.auc * 0.96)
  ci.lower <- auc - (se.auc * 0.96)

  se.auc.null <- sqrt((1 + length(pos) + length(neg))/(12*length(pos)*length(neg)))
  z <- (auc - 0.5)/se.auc.null
  p <- 2*pnorm(-abs(z))

  stats <- data.frame (auc = auc,
                       p.value = p,
                       ci.upper = ci.upper,
                       ci.lower = ci.lower
                       )

  return (list(roc = roc, stats = stats))
}

##' Plot a ROC Curve Using ggplot2
##'
##' Plot an ROC curve using ggplot2
##' @title rocplot.single
##' @param grp factor with two levels coding the group membership
##' @param pred numeric variable that should be used predict the group membership
##' @param title title
##' @param p.value boolean.  if TRUE print the p-value onto the plot, else print the confidence interval
##' @param youden boolen.  if TRUE the youden index is marked and annotated with the corresponding cutoff
##' @return ggpolot object
##' @author Kate Nambiar \email{k.z.nambiar@@bsms.ac.uk} with modifications by Andreas Leha
##' \url{http://www.rrandomness.com/r/simple-roc-plots-with-ggplot2-part-1/}
##' @export
##' @examples
##' if (require(survival))
##'   rocplot.single(pbc$edema, pbc$albumin)
rocplot.single <- function(grp, pred, title = "ROC Plot", p.value = FALSE, youden=FALSE) {
  plotdata <- rocdata(grp, pred)

  if (p.value == TRUE){
    annotation <- with(plotdata$stats, paste("AUC=",signif(auc, 2), " (P=", signif(p.value, 2), ")", sep=""))
  } else {
    annotation <- with(plotdata$stats, paste("AUC=",signif(auc, 2), " (95%CI ", signif(ci.upper, 2), " - ", signif(ci.lower, 2), ")", sep=""))
  }

  p <- ggplot(plotdata$roc, aes_string(x = "x", y = "y")) +
    geom_line(aes(colour = "")) +
    ##geom_abline (intercept = 0, slope = 1, colour="gray") +
    geom_segment(aes(x=0, y=0, xend=1, yend=1), colour="gray") +
    theme_bw() +
    scale_x_continuous("False Positive Rate (1-Specificity)") +
    scale_y_continuous("True Positive Rate (Sensitivity)") +
    scale_colour_manual(labels = annotation, values = "#000000") +
    ggtitle(title) +
    theme(plot.title = element_text(face="bold", size=14),
          axis.title.x = element_text(face="bold", size=12),
          axis.title.y = element_text(face="bold", size=12, angle=90),
          ##panel.grid.major = element_blank(),
          ##panel.grid.minor = element_blank(),
          legend.justification=c(1,0),
          legend.position=c(1,0),
          legend.title=element_blank(),
          legend.key = element_blank()
          )

  if (youden) {
    p <- p + geom_point(aes_string(x="fpr", y="tpr"), data=as.data.frame(plotdata$youden))
    youden_annot <- data.frame(fpr=plotdata$youden[["fpr"]],
                               tpr=plotdata$youden[["tpr"]],
                               label=paste("youden cutoff", plotdata$youden[["cutoff"]]))
    p <- p + geom_text(aes_string(x="fpr", y="tpr", label="label"),
                       data=youden_annot,
                       size=4,
                       hjust=0,
                       vjust=1)
  }

  return(p)
}

##' Plot Multiple ROC Curves onto One Canvas
##'
##' Plot Multiple ROC Curves onto One Canvas Using ggplot2
##' @title rocplot.multiple
##' @param test.data.list list of lists.  each element is a list containing (at least) the class and a numeric predictor
##' @param groupName string.  name of the class variable in the elements of \code{test.data.list}
##' @param predName string.  name of the predictor variable in the elements of \code{test.data.list}
##' @param title plot title
##' @param p.value boolean.  if TRUE print the p-value onto the plot, else print the confidence interval
##' @return ggplot2 object
##' @author Kate Nambiar \email{k.z.nambiar@@bsms.ac.uk}
##' \url{http://www.rrandomness.com/r/simple-roc-plots-with-ggplot2-part-1/}
##' @export
##' @examples
##' if (require(survival))
##'   rocplot.multiple(list(albumin=list(grp=pbc$edema, res=pbc$albumin),
##'                         bili=list(grp=pbc$edema, res=pbc$bili)))
rocplot.multiple <- function(test.data.list, groupName = "grp", predName = "res", title = "ROC Plot", p.value = TRUE){
  plotdata <- llply(test.data.list, function(x) with(x, rocdata(grp = eval(parse(text = groupName)), value = eval(parse(text = predName)))))
  plotdata <- list(roc = ldply(plotdata, function(x) x$roc),
                   stats = ldply(plotdata, function(x) x$stats)
                   )

  if (p.value == TRUE){
    annotation <- with(plotdata$stats, paste("AUC=",signif(auc, 2), " (P=", signif(p.value, 2), ")", sep=""))
  } else {
    annotation <- with(plotdata$stats, paste("AUC=",signif(auc, 2), " (95%CI ", signif(ci.upper, 2), " - ", signif(ci.lower, 2), ")", sep=""))
  }

  p <- ggplot(plotdata$roc, aes_string(x = "x", y = "y")) +
       geom_line(aes_string(colour = ".id")) +
       geom_abline (intercept = 0, slope = 1) +
       theme_bw() +
       scale_x_continuous("False Positive Rate (1-Specificity)") +
       scale_y_continuous("True Positive Rate (Sensitivity)") +
       scale_colour_brewer(palette="Set1", breaks = names(test.data.list), labels = paste(names(test.data.list), ": ", annotation, sep = "")) +
       ggtitle(title) +
       theme(plot.title = element_text(face="bold", size=14),
             axis.title.x = element_text(face="bold", size=12),
             axis.title.y = element_text(face="bold", size=12, angle=90),
             panel.grid.major = element_blank(),
             panel.grid.minor = element_blank(),
             legend.justification=c(1,0),
             legend.position=c(1,0),
             legend.title=element_blank(),
             legend.key = element_blank()
             )

  return(p)
}


##' Grid of Plots in ggplot2
##'
##' Arrange multiple plots in a simple table layout in ggplot2.
##' @title muliplot
##' @param ... plots passed one by one
##' @param plotlist plots passed in a list (appended to ...)
##' @param cols number of cols in the plots table
##' @param widths numeric vector or unit object describing the widths of the
##          columns in the layout.
##' @param heights numeric vector or unit object describing the heights of the
##          columns in the layout.
##' @return none
##' @author Winston Chang
##' \url{http://wiki.stdout.org/rcookbook/Graphs/Multiple graphs on one page (ggplot2)/}
##' @export
##' @examples
##' tmpp1 <- ggheatmap(matrix(rnorm(50*10), 50, 10))
##' tmpp2 <- ggheatmap(matrix(rnorm(50*10), 50, 10))
##' multiplot(tmpp1, tmpp2, cols=1)
multiplot <- function(..., plotlist=NULL, cols, widths, heights) {

  ## Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)

  numPlots = length(plots)

  ## Make the panel
  plotCols = cols                          # Number of columns of plots
  plotRows = ceiling(numPlots/plotCols) # Number of rows needed, calculated from # of cols

  ## set widths / heights
  if (missing(widths))
    widths <- unit(rep_len(1, plotCols), "null")
  if (missing(heights))
    heights <- unit(rep_len(1, plotRows), "null")


  ## Set up the page
  grid.newpage()
  pushViewport(viewport(layout = grid.layout(plotRows, plotCols, widths=widths, heights=heights)))
  vplayout <- function(x, y)
    viewport(layout.pos.row = x, layout.pos.col = y)

  ## Make each plot, in the correct location
  for (i in 1:numPlots) {
    curRow = ceiling(i/plotCols)
    curCol = (i-1) %% plotCols + 1
    print(plots[[i]], vp = vplayout(curRow, curCol ))
  }
}


##' Print Survival Times with indication of censoring and possible grouping.
##'
##' @title Print Survival Times
##' @param S Survival object
##' @param grouping optional. vector of length nrow(S). containing group membership of the patients
##' @param patnames optional. character vector of length nrow(S). names of patients
##' @param printpatnames optional. boolean.  print names of patients? (default: FALSE)
##' @param xbreaks optional. numeric vector.  where to put the breaks on the time axis.
##' @param dimname_time optional.  character.  xlab
##' @param dimname_patient optional.  character. ylab
##' @param dimname_grouping optional. character. legend title
##' @return ggplot object
##' @author Andreas Leha
##' @export
ggsurv <- function(S, grouping, patnames, printpatnames, xbreaks, dimname_time, dimname_patient, dimname_grouping) {
  if (missing(dimname_time))
    dimname_time <- "Time"
  if (missing(dimname_patient))
    dimname_patient <- "Patient"
  if (missing(dimname_grouping))
    dimname_grouping <- "Grouping"

  if (missing(patnames))
    patnames <- paste0("patient", sprintf(paste0("%0", nchar(as.character(nrow(S))), "d"), 1:nrow(S)))
  if (missing(printpatnames))
    printpatnames <- FALSE

  survtimedf <- data.frame(time=S[,"time"],
                           event=as.logical(S[,"status"]),
                           patient=factor(patnames, levels=rev(patnames)))

  if (missing(xbreaks))
    xbreaks <- labeling::extended(0, max(survtimedf$time, na.rm=TRUE), 6)

  xbreaksdf <- data.frame(xbreaks=xbreaks[-1])

  if (!missing(grouping))
    survtimedf <- cbind(survtimedf, groupingdf=as.factor(grouping))

  NA_idx <- is.na(S)

  p <- ggplot(survtimedf) +
    geom_segment(aes(y=patient, yend=patient,
                     x=ifelse(time<0, time, 0), xend=ifelse(time==0, 0.3, time))) +
      geom_vline(aes(xintercept=xbreaks), data=xbreaksdf, col="white", lwd=1) +
        geom_point(data=survtimedf[survtimedf$event & !NA_idx,], aes(y=patient, x=time), shape="+", size=3) +
          ylab(dimname_patient) +
            xlab(dimname_time) +
              theme_bw() +
                scale_x_continuous(breaks=xbreaks, expand=c(0,0.1)) +
                  theme(strip.text.x = element_text(size=12, face="bold", colour="grey20"),
                        strip.text.y = element_text(size=12, face="bold", colour="grey20"),
                        strip.background = element_rect(colour="grey50", fill="grey80")) +
                          theme(panel.grid.minor = element_blank(),
                                panel.grid.major = element_blank()) +
                                  theme(axis.ticks.y = element_blank())

  if (!missing(grouping)) p <- p + facet_grid(groupingdf ~ ., scales="free", space="free")

  if(!printpatnames) p <- p + theme(axis.text.y=element_blank())


  p
}


##' Plot a data.frame in the style of LaTeX booktabs
##'
##' There exist other facilities for plotting tables in grid.  See, for
##' instance, \code{\link{tableGrob}} from the gridExtra package.
##' This function differs by making it easy to plot the table in the style
##' of LaTeX booktabs.  Its main use is for adding a table properly alinged
##' to a plot.
##'
##' @param ggdf data.frame.  This is what will be plotted
##' @param rownames logical or character.  If FALSE, no rownames will
##' be printed.  If TRUE, the rownames of ggdf will be used as
##' rownames.  If character, the colname of one column in ggdf, which will
##' be used as rownames.  If you want to enforce a specific order on the
##' rows of the table, the given column should be a factor with the levels
##' in that order.  Defaults to FALSE.
##' @param plots logical.  If TRUE a plot will be displayed as side-effect.
##' Defaults to TRUE.
##' @param fontsize numeric.  The fontsize for the table contents in points.
##' Defaults to 5.
##' @param ylab logical.  If TRUE the rownames will be added to the table.
##' Defaults to TRUE.
##' @return The constructed plot.
##' This will be of class c("gtable", "grob", "gDesc").  Use grid.draw()
##' to actually plot this.
##' @author Andreas Leha
##' @export
ggbooktabs <- function(ggdf, rownames=FALSE, plots=TRUE, fontsize=5, ylab=TRUE)
{
  if (!is.data.frame(ggdf))
      ggdf <- as.data.frame(ggdf)

  if (is.logical(rownames)) {
    if (rownames) {
      ggdf <- name_rows(ggdf)
      ggdf$.rownames <- factor(ggdf$.rownames, levels=rev(ggdf$.rownames))
      ggdf <- ggdf[,c(ncol(ggdf),1:(ncol(ggdf)-1))]
      colnames(ggdf)[1] <- " "
      rownames=" "
    }
  } else {
    idx_rownames <- which(colnames(ggdf) == rownames)
    ggdf <- ggdf[,c(idx_rownames, setdiff(1:ncol(ggdf), idx_rownames))]
  }

  ## make everything character
  for (j in 1:ncol(ggdf)) {
    ggdf[,j] <- as.character(ggdf[,j])
  }

  ggdf$ggbooktabs_rownames <- factor(1:nrow(ggdf), levels=nrow(ggdf):1)
  ggdf_plot <- reshape::melt.data.frame(ggdf, id.vars = "ggbooktabs_rownames", factorsAsStrings = TRUE)

  ## measure the string lengths
  ##ggdf_plot$value_strwidth <- stringWidth(ggdf_plot$value)
  ##ggdf_plot$variable_strwidth <- stringWidth(ggdf_plot$variable)
  ##do.call(unit.c, dlply(ggdf_plot, "variable",
  ##                      function(x)
  ##                    {
  ##                      max(max(x$value_strwidth),
  ##                          max(x$variable_strwidth))
  ##                    }))

  ## count chars in the strings
  colwidths <- daply(ggdf_plot,
                     "variable",
                     function(x) max(c(nchar(as.character(x$value)),
                                       nchar(as.character(x$variable)))))
  colends <- cumsum(colwidths)
  ggdf_plot$colends <- rep(colends, daply(ggdf_plot, "variable", nrow))


  ## left-align the rownames
  if (is.character(rownames)) {
    ggdf_plot$isrowname <- ifelse(ggdf_plot$variable == rownames, 0, 1)
    ggdf_plot$colends <- ifelse(ggdf_plot$variable == rownames,
                                0,
                                ggdf_plot$colends)
  } else {
    ggdf_plot$isrowname <- 1
  }
  ## do the plotting
  ## the equidistant version
  ##plot_booktabs <-
  ##    ggplot(ggdf_plot,
  ##           aes_string(y=rownames, x="variable")) +
  ##  geom_text(aes_string(label="value"), hjust=1, size=fontsize) +
  ##  geom_text(x=2, y=1, alpha=0, label="95% confidence interval", hjust=1) +
  ##  theme_minimal() +
  ##  theme(##axis.text.x=element_text(hjust = 1, size=18),
  ##        panel.grid.minor=element_blank(),
  ##        panel.grid.major=element_blank(),
  ##        axis.ticks=element_blank(),
  ##        axis.title.x=element_blank(),
  ##        axis.title.y=element_blank(),
  ##        ##axis.line = element_line(color = 'black'),
  ##        axis.line.y = element_blank(),
  ##        axis.text.x=element_blank(),
  ##        plot.margin=unit(c(2, 0.5, 0.5, 0.5), "lines"))

  ## do the plotting again
  ## the nchar based version
  plot_booktabs <-
  ggplot(ggdf_plot,
         aes_string(y="ggbooktabs_rownames", x="colends")) +
    geom_text(aes_string(label="value", hjust="isrowname"), size=fontsize) +
    geom_text(x=2, y=1, alpha=0, label="95% confidence interval", hjust=1) +
    scale_x_continuous(expand = c(0,0.5),
                       limits=c(0, max(colends)),
                       breaks=colends) +
    theme_minimal() +
    theme(##axis.text.x=element_text(hjust = 1, size=18),
          panel.grid.minor=element_blank(),
          panel.grid.major=element_blank(),
          axis.ticks=element_blank(),
          axis.title.x=element_blank(),
          axis.title.y=element_blank(),
          ##axis.line = element_line(color = 'black'),
          axis.line.y = element_blank(),
          axis.text.x=element_blank(),
          axis.text.y=element_blank(),
          plot.margin=unit(c(2, 0.5, 0.5, 0.5), "lines"))

  ## add the table header
  plot_booktabs_w_header <- plot_booktabs
  for (i in 1:length(levels(as.factor(ggdf_plot$variable))))
  {
    lab <- levels(as.factor(ggdf_plot$variable))[i]
    plot_booktabs_w_header <-
    plot_booktabs_w_header +
      annotation_custom(
          grob = textGrob(label = lab,
              hjust = (if (is.character(rownames) && lab==rownames) 0 else 1),
              gp = gpar(cex = 0.8, fontface="bold")),
          xmin = (if (is.character(rownames) && lab==rownames) 0 else colends[i]),                # Horizontal position of the textGrob
          xmax = (if (is.character(rownames) && lab==rownames) 0 else colends[i]),
          ymin = nrow(ggdf) + 1.2, # Note: The grobs are positioned outside the plot area
          ymax = nrow(ggdf) + 1.2)
  }

  ## add the hlines
  ## the midline
  plot_booktabs_w_header <- plot_booktabs_w_header +
      annotation_custom(
          grob = linesGrob(x=c(0.01, 0.99), y=c(0,0), gp=gpar(lwd=0.7)),
          xmin = -Inf, xmax = Inf,
          ymin = nrow(ggdf) + 0.6,
          ymax = nrow(ggdf) + 0.6)
  ## the topline
  plot_booktabs_w_header <- plot_booktabs_w_header +
      annotation_custom(
          grob = linesGrob(x=c(0, 1), y=c(0,0)),
          xmin = -Inf, xmax = Inf,
          ymin = nrow(ggdf) + 1.8,
          ymax = nrow(ggdf) + 1.8)
  ## the bottomline
  plot_booktabs_w_header <- plot_booktabs_w_header +
      annotation_custom(
          grob = linesGrob(x=c(0, 1), y=c(0,0)),
          xmin = -Inf, xmax = Inf,
          ymin = 0.4,
          ymax = 0.4)


  if (!ylab)
      plot_booktabs_w_header <- plot_booktabs_w_header +
          theme(axis.text.y=element_blank())

  ## Code to override clipping
  gt <- ggplot2::ggplot_gtable(ggplot2::ggplot_build(plot_booktabs_w_header))
  gt$layout$clip[gt$layout$name == "panel"] <- "off"

  if (plots) {
    grid.newpage()
    grid.draw(gt)
  }

  invisible(gt)
}

##' Combine several grid plots while alinging the plot ares
##'
##' The plots passed to this function will be aligned, such that the
##' plotting araes are aligned.  Most useful if the scales match.
##'
##' @param ... the plots.  Must be grid graphics.  Tested with objects of
##' class "ggplot" and "gtable".
##' @param direction character in c("horizontal", "vertical").  The direction
##' in which to combine the plots.
##' Defaults to "horizontal".
##' @param plots logical.  If TRUE, a plot will be generated as side effect.
##' Defaults to TRUE.
##' @param widths numerical vector.  Only effective, if direction=="horizontal".
##' The relative sizes of the plots to
##' combine.  If NULL, each plot is given the same amount of space.
##' Defaults to NULL.
##' @param heights numerical vector.  Only effective, if direction=="vertical".
##' The relative sizes of the plots to
##' combine.  If NULL, each plot is given the same amount of space.
##' Defaults to NULL.
##' @return The return value of arrangeGrob: a frame grob.
##' @author Andreas Leha
##' @export
ggcombine <- function(..., direction="horizontal", plots=TRUE, widths=NULL, heights=NULL)
{
  plot_list <- list(...)
  plot_list_gtable <-
      llply(plot_list, function(ggp) if (is.ggplot(ggp)) ggplot2::ggplot_gtable(ggplot2::ggplot_build(ggp)) else ggp)
  if (direction=="horizontal") {
    plot_list_height <- llply(plot_list_gtable, '[[', "heights")
    plot_list_height <- llply(plot_list_height, '[', 2:5)

    maxHeight = do.call(grid::unit.pmax, plot_list_height)

    plot_list_gtable <-
        llply(plot_list_gtable, function(x)
            {
              x$heights[2:5] <- as.list(maxHeight)
              x
            })

    if (is.null(widths)) widths <- rep(1, length(plot_list_gtable))

    plot_combined <-
        do.call(arrangeGrob,
                c(plot_list_gtable,
                  list(ncol=length(plot_list_gtable),
                       widths=widths)))
  } else if (direction=="vertical") {
    plot_list_width <- llply(plot_list_gtable, '[[', "widths")
    plot_list_width <- llply(plot_list_width, '[', 2:5)

    maxWidth = do.call(grid::unit.pmax, plot_list_width)

    plot_list_gtable <-
        llply(plot_list_gtable, function(x)
            {
              x$widths[2:5] <- as.list(maxWidth)
              x
            })

    if (is.null(heights)) heights <- rep(1, length(plot_list_gtable))

    plot_combined <-
        do.call(arrangeGrob,
                c(plot_list_gtable,
                  list(nrow=length(plot_list_gtable),
                       heights=heights)))

  } else {
    stop("direction ", direction, "not supported.  Only 'horizontal' and 'vertical' allowed")
  }

  if (plots) print(plot_combined)

  plot_combined
}







rbind_gtable_max <- function(...){

  gtl <- list(...)
  ##stopifnot(all(sapply(gtl, is.gtable)))
  bind2 <- function (x, y)
  {
    stopifnot(ncol(x) == ncol(y))
    if (nrow(x) == 0)
      return(y)
    if (nrow(y) == 0)
      return(x)
    y$layout$t <- y$layout$t + nrow(x)
    y$layout$b <- y$layout$b + nrow(x)
    x$layout <- rbind(x$layout, y$layout)
    x$heights <- gtable:::insert.unit(x$heights, y$heights)
    x$rownames <- c(x$rownames, y$rownames)
    x$widths <- grid::unit.pmax(x$widths, y$widths)
    x$grobs <- append(x$grobs, y$grobs)
    x
  }

  Reduce(bind2, gtl)
}

cbind_gtable_max <- function(...){

  gtl <- list(...)
  ##stopifnot(all(sapply(gtl, is.gtable)))
  bind2 <- function (x, y)
  {
    stopifnot(nrow(x) == nrow(y))
    if (ncol(x) == 0)
      return(y)
    if (ncol(y) == 0)
      return(x)
    y$layout$l <- y$layout$l + ncol(x)
    y$layout$r <- y$layout$r + ncol(x)
    x$layout <- rbind(x$layout, y$layout)
    x$widths <- gtable:::insert.unit(x$widths, y$widths)
    x$colnames <- c(x$colnames, y$colnames)
    x$heights <- grid::unit.pmax(x$heights, y$heights)
    x$grobs <- append(x$grobs, y$grobs)
    x
  }
  Reduce(bind2, gtl)
}

##' Combine several grid plots while alinging the plot ares
##'
##' The plots passed to this function will be aligned, such that the
##' plotting araes are aligned.  Most useful if the scales match.
##'
##' @param ... the plots.  Must be grid graphics.  Tested with objects of
##' class "ggplot" and "gtable".
##' @param direction character in c("horizontal", "vertical").  The direction
##' in which to combine the plots.
##' Defaults to "horizontal".
##' @param plots logical.  If TRUE, a plot will be generated as side effect.
##' Defaults to TRUE.
##' @param widths numerical vector.  Only effective, if direction=="horizontal".
##' The relative sizes of the plots to
##' combine.  If NULL, each plot is given the same amount of space.
##' Defaults to NULL.
##' @param heights numerical vector.  Only effective, if direction=="vertical".
##' The relative sizes of the plots to
##' combine.  If NULL, each plot is given the same amount of space.
##' Defaults to NULL.
##' @return The return value of arrangeGrob: a frame grob.
##' @author Andreas Leha
##' @export
ggcombine.2 <- function(..., direction="horizontal", plots=TRUE, widths=NULL, heights=NULL)
{
  plot_list <- list(...)
  plot_list_gtable <-
      llply(plot_list, function(ggp) if (is.ggplot(ggp)) ggplot_gtable(ggplot_build(ggp)) else ggp)
  if (direction=="horizontal") {

    plot_combined <- do.call(cbind_gtable_max, plot_list_gtable)

  } else if (direction=="vertical") {

    plot_combined <- do.call(rbind_gtable_max, plot_list_gtable)

  } else {
    stop("direction ", direction, "not supported.  Only 'horizontal' and 'vertical' allowed")
  }

  if (plots) print(plot_combined)

  plot_combined
}
