# Handy Functions for People Working with ggplot2

This is a bag of miscellaneous functions that support the work with
the ggplot2 R-package.

It needs a lot of cleaning, though.  Especially since it loads some
packages that mask ddplyr functions.


## Installation

Best installed with the `devtools` package:

    library("devtools")
    install_git("https://gitlab.com/al22/gghandy.git")
